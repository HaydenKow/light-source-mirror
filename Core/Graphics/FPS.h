#ifndef LIGHT_SOURCE_CORE_GRAPHICS_FPS_H
#define LIGHT_SOURCE_CORE_GRAPHICS_FPS_H

#include "../System/Timer.h"

namespace LightSource { namespace Core { namespace Graphics {

    class FPS
    {
    private:
        u64 m_curr_tick;
        u64 m_last_tick;
        
        u32 m_tick_resolution;

        float m_curr_ms;
        float m_curr_fps;

        int m_frame_count;
    public:
        FPS();
        ~FPS();

        float GetLatency() const;
        float GetCurrentFPS() const;

        void PreUpdate();
        void PostUpdate();
    };

}}} // LightSource::Core::Graphics

#endif