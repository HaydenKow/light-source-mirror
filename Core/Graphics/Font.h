#ifndef LIGHT_SOURCE_CORE_GRAPHICS_FONT_H
#define LIGHT_SOURCE_CORE_GRAPHICS_FONT_H

#include <../_Libs_/intraFont.h>

extern intraFont* light_source_core_graphics_font;

namespace LightSource { namespace Core { namespace Graphics {

    class Font
    {
    private:
        bool Init();
        void Shutdown();

    public:
        Font(const char* file, unsigned int options);
        ~Font();

        intraFont* GetFont() const;

        void SetStyle(float size, unsigned int color, unsigned int shadowColor, unsigned int options);

        float Print(float x, float y, const char *text);
        
        template<class... Args>
        float Printf(float x, float y, const char* format, Args... args)
        {
            return intraFontPrintf(light_source_core_graphics_font, x, y, format, args...);
        }
    };

}}} // LightSource::Core::Graphics

#endif