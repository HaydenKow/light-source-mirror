#include "Font.h"

intraFont* light_source_core_graphics_font = NULL;

namespace LightSource { namespace Core { namespace Graphics {

    Font::Font(const char* file, unsigned int options)
    {
        light_source_core_graphics_font = intraFontLoad(file, options);
        this->Init();
    }

    Font::~Font()
    {
        intraFontUnload(light_source_core_graphics_font);

        this->Shutdown();
    }

    bool Font::Init()
    {
        return intraFontInit() == 1;
    }

    void Font::Shutdown()
    {
        intraFontShutdown();
    }


    intraFont* Font::GetFont() const
    {
        return light_source_core_graphics_font;
    }

    void Font::SetStyle(float size, unsigned int color, unsigned int shadowColor, unsigned int options)
    {
        intraFontSetStyle(light_source_core_graphics_font, size, color, shadowColor, 0, options);
    }

    float Font::Print(float x, float y, const char *text)
    {
        return intraFontPrint(light_source_core_graphics_font, x, y, text);
    }

}}} // LightSource::Core::Graphics