#ifndef LIGHT_SOURCE_CORE_GRAPHICS_CAMERA_H
#define LIGHT_SOURCE_CORE_GRAPHICS_CAMERA_H

#include <psptypes.h>
//#include <stdlib.h>

namespace LightSource { namespace Core { namespace Graphics {

    class Camera
    {
    private:
        ScePspFVector3 m_position;

    public:
        Camera();
        Camera(float x, float y, float z);
        virtual ~Camera();

        void SetPositionX(float x);
        void SetPositionY(float y);
        void SetPositionZ(float z);
        void SetPosition(float x, float y, float z);

        float GetPositionX() const;
        float GetPositionY() const;
        float GetPositionZ() const;
        ScePspFVector3* GetPosition();
    };

}}} // LightSource::Core::Graphics

#endif