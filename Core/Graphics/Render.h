#ifndef LIGHT_SOURCE_CORE_GRAPHICS_RENDER_H
#define LIGHT_SOURCE_CORE_GRAPHICS_RENDER_H

#include <pspthreadman.h>
#include <psptypes.h>

#include <pspgu.h>
#include <pspgum.h>

#include "Display.h"
#include "Vram.h"
#include "FPS.h"

namespace LightSource { namespace Core { namespace Graphics {
    
    class Render
    {
    private:
        LightSource::Core::Graphics::Display*   m_display;
        LightSource::Core::Graphics::FPS*       m_fps;

        unsigned int                            m_displayList[262144] __attribute__((aligned(16)));

        void*                                   m_frameBufferPointerFirst;
        void*                                   m_frameBufferPointerSecond;
        void*                                   m_depthBufferPointer;
        void*                                   m_cacheBufferPointer;

        //cpu and gpu timing
        bool m_performanceCounter;
        int m_g_vbl_count;                  // previous vblank count
        int m_g_vbl_step;                   // previous vblank step
        int m_g_vbl_time;                   // previous vblank time
        int m_g_cpu_load;                   // current cpu load
        int m_g_gpu_load;                   // current gpu load
        int m_g_frame_count;

        int m_tmp_cpu_time;                 // tmp
        int m_tmp_gpu_time;                 // tmp
        int m_tmp_vbl_time;                 // tmp
        int m_tmp_vbl_count;                // tmp

    public:
        Render();
        virtual ~Render();

        LightSource::Core::Graphics::Display* Display() const;
        LightSource::Core::Graphics::FPS* FPS() const;

        void* GetFrameBufferPointerFirst() const;
        void* GetFrameBufferPointerSecond() const;
        void* GetDepthBufferPointer() const;
        void* GetCacheBufferPointer() const;

        void Init();
        void FrameStart();
        void FrameClear();
        void FrameEnd();

        void BeginObject(int vtype, int count, const void* indices, const void* vertices);
        void DrawArray(int prim, int vtype, int count, const void* indices, const void* vertices);
        void EndObject();

        void Dispose();
    };
}}} // LightSource::Core::Graphics

#endif