#include "Display.h"

namespace LightSource { namespace Core { namespace Graphics {

    Display::Display(int buffWidth, int widht, int height) 
    {
        this->SetBufferWidth(buffWidth);
        this->SetWidth(widht);
        this->SetHeight(height);

        pspDebugScreenInit();
    }

    Display::~Display()
    {
    }

    void Display::SetBufferWidth(int buffWidth = 512)
    {
        m_buffWidth = buffWidth;
    }

    void Display::SetWidth(int widht = 480)
    {
        m_width = widht;
    }

    void Display::SetHeight(int height = 272)
    {
        m_height = height;
    }

    void Display::SetXY(int x, int y)
    {
        pspDebugScreenSetXY(x, y);
    }

    int Display::GetBufferWidth()
    {
        return m_buffWidth;
    }

    int Display::GetWidth()
    {
        return m_width;
    }

    int Display::GetHeight()
    {
        return m_height;
    }

    int Display::WaitVblankStart()
    {
        return sceDisplayWaitVblankStart();
    }

    void Display::DebugClear()
    {
        pspDebugScreenClear();
    }

}}} // LightSource::Core::Graphics