#include "Camera.h"

namespace LightSource { namespace Core { namespace Graphics {

    Camera::Camera()
    {
    }

    Camera::Camera(float x, float y, float z)
    {
        this->SetPosition(x, y, z);
    }

    Camera::~Camera()
    {
    }

    void Camera::SetPositionX(float x)
    {
        m_position.x = x;
    }
    void Camera::SetPositionY(float y)
    {
        m_position.y = y;
    }
    void Camera::SetPositionZ(float z)
    {
        m_position.z = z;
    }
    void Camera::SetPosition(float x, float y, float z)
    {
        this->SetPositionX(x);
        this->SetPositionY(y);
        this->SetPositionZ(z);
    }

    float Camera::GetPositionX() const
    {
        return m_position.x;
    }

    float Camera::GetPositionY() const
    {
        return m_position.y;
    }

    float Camera::GetPositionZ() const
    {
        return m_position.z;
    }

    ScePspFVector3* Camera::GetPosition()
    {
        return &m_position;
    }

}}} // LightSource::Core::Graphics