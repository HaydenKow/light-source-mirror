#include "Render.h"

namespace LightSource { namespace Core { namespace Graphics {

    Render::Render()
    {
        this->m_display = new LightSource::Core::Graphics::Display();
        this->m_fps = new LightSource::Core::Graphics::FPS();

        this->m_frameBufferPointerFirst = VideoRam::AllocBuffer(this->m_display->GetBufferWidth(), this->m_display->GetHeight(), GU_PSM_8888);
        this->m_frameBufferPointerSecond = VideoRam::AllocBuffer(this->m_display->GetBufferWidth(), this->m_display->GetHeight(), GU_PSM_8888);
        this->m_depthBufferPointer = VideoRam::AllocBuffer(this->m_display->GetBufferWidth(), this->m_display->GetHeight(), GU_PSM_4444);
        this->m_cacheBufferPointer = VideoRam::GetAddress();

        this->m_performanceCounter = true;
    }

    Render::~Render()
    {
    }

    LightSource::Core::Graphics::Display* Render::Display() const
    {
        return this->m_display;
    }

    LightSource::Core::Graphics::FPS* Render::FPS() const
    {
        return this->m_fps;
    }

    void* Render::GetFrameBufferPointerFirst() const
    {
        return this->m_frameBufferPointerFirst;
    }

    void* Render::GetFrameBufferPointerSecond() const
    {
        return this->m_frameBufferPointerSecond;
    }

    void* Render::GetDepthBufferPointer() const
    {
        return this->m_depthBufferPointer;
    }

    void* Render::GetCacheBufferPointer() const
    {
        return this->m_cacheBufferPointer;
    }


    void Render::Init()
    {
        sceGuInit();

        sceGuStart(GU_DIRECT, this->m_displayList);
        
        sceGuDrawBuffer(GU_PSM_8888, this->m_frameBufferPointerFirst, this->m_display->GetBufferWidth());
        sceGuDispBuffer(this->m_display->GetWidth(), this->m_display->GetHeight(), this->m_frameBufferPointerSecond, this->m_display->GetBufferWidth());
        sceGuDepthBuffer(this->m_depthBufferPointer, this->m_display->GetBufferWidth());
        
        sceGuOffset(2048 - (this->m_display->GetWidth()/2),2048 - (this->m_display->GetHeight()/2));
        sceGuViewport(2048, 2048, this->m_display->GetWidth(), this->m_display->GetHeight());
        sceGuDepthRange(65535, 0);
        
        sceGuScissor(0, 0, this->m_display->GetWidth(), this->m_display->GetHeight());
        sceGuEnable(GU_SCISSOR_TEST);
        
        sceGuDepthFunc(GU_GEQUAL);
        sceGuEnable(GU_DEPTH_TEST);

        sceGuEnable(GU_BLEND);
        sceGuBlendFunc(GU_ADD, GU_SRC_ALPHA, GU_ONE_MINUS_SRC_ALPHA, 0, 0);
        sceGuAlphaFunc(GU_GREATER, 0.0f, 0xff );

        //sceGuStencilFunc(GU_ALWAYS, 1, 1); // always set 1 bit in 1 bit mask
	    //sceGuStencilOp(GU_KEEP, GU_KEEP, GU_REPLACE); // keep value on failed test (fail and zfail) and replace on pass
        
        sceGuFrontFace(GU_CCW);
        sceGuShadeModel(GU_SMOOTH);
        
        sceGuEnable(GU_CULL_FACE);
        sceGuEnable(GU_TEXTURE_2D);
        sceGuEnable(GU_CLIP_PLANES);
        
        sceGuFinish();
        sceGuSync(0,0);

        this->m_display->WaitVblankStart();

        sceGuDisplay(GU_TRUE);
    }

    void Render::FrameStart()
    {
        sceGuStart(GU_DIRECT, this->m_displayList);
    }

    void Render::FrameClear()
    {
        sceGuClearColor(0xff554433);
        sceGuClearDepth(0);
        sceGuClear(GU_COLOR_BUFFER_BIT|GU_DEPTH_BUFFER_BIT);
    }

    void Render::FrameEnd()
    {
        if(!this->m_performanceCounter) {
            sceGuFinish();
            sceGuSync(0, 0);

            sceDisplayWaitVblankStart();
            sceGuSwapBuffers();
        } else {
            sceGuFinish();
            this->m_tmp_cpu_time = sceKernelGetSystemTimeLow();
            sceGuSync(0, 0);
            this->m_tmp_gpu_time = sceKernelGetSystemTimeLow();
            sceDisplayWaitVblankStart();
            sceGuSwapBuffers();

            this->m_g_frame_count++;

            //performance
            this->m_tmp_vbl_time = sceKernelGetSystemTimeLow();
            this->m_tmp_vbl_count = sceDisplayGetVcount();

            if ( this->m_g_vbl_count >= 0 )
            {
                this->m_g_vbl_step = this->m_tmp_vbl_count - this->m_g_vbl_count ;
                if ( this->m_g_frame_count % 6 == 0 )
                {
                    this->m_g_cpu_load = ( this->m_tmp_cpu_time - this->m_g_vbl_time ) * 100 / 16667 ;
                    this->m_g_gpu_load = ( this->m_tmp_gpu_time - this->m_g_vbl_time ) * 100 / 16667 ;
                }
            }
            this->m_g_vbl_time = this->m_tmp_vbl_time ;
            this->m_g_vbl_count = this->m_tmp_vbl_count ;
        }
    }

    void Render::BeginObject(int vtype, int count, const void* indices, const void* vertices)
    {
        return sceGuBeginObject(vtype, count, indices, vertices);
    }

    void Render::DrawArray(int prim, int vtype, int count, const void* indices, const void* vertices)
    {
        return sceGumDrawArray(prim, vtype, count, indices, vertices);
    }

    void Render::EndObject()
    {
        return sceGuEndObject();
    }

    void Render::Dispose()
    {
        sceGuTerm();
    }

}}} // LightSource::Core::Graphics