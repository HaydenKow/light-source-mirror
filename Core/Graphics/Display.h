#ifndef LIGHT_SOURCE_GRAPHICS_DISPLAY_H
#define LIGHT_SOURCE_GRAPHICS_DISPLAY_H

#include <pspdisplay.h>
#include <pspdebug.h>                   // pspDebugScreenInit, pspDebugScreenPrintf

namespace LightSource { namespace Core { namespace Graphics {
            
    class Display
    {
    private:
        int m_buffWidth;
        int m_width;
        int m_height;

    public:
        Display(int buffWidth = 512, int widht = 480, int height = 272);
        virtual ~Display();

        void SetBufferWidth(int buffWidth);
        void SetWidth(int widht);
        void SetHeight(int height);
        void SetXY(int x, int y);

        int GetBufferWidth();
        int GetWidth();
        int GetHeight();

        int WaitVblankStart();
        
        void DebugClear();

        template<class... Args>
        void Printf(const char* format, Args... args)
        {
            pspDebugScreenPrintf(format, args...);
        }
    };

}}} // LightSource::Core::Graphics

#endif