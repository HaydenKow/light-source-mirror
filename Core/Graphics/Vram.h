#ifndef LIGHT_SOURCE_CORE_GRAPHICS_VRAM_H
#define LIGHT_SOURCE_CORE_GRAPHICS_VRAM_H

#include <pspge.h>
#include <pspgu.h>

namespace LightSource { namespace Core { namespace Graphics {
    
    class VideoRam
    {
    private:
        static unsigned int m_staticOffset;
        static unsigned int GetMemorySize(unsigned int width, unsigned int height, unsigned int psm);

    public:
        static void* GetAddress();

        static void* AllocBuffer(unsigned int width, unsigned int height, unsigned int psm);
        static void* AllocTexture(unsigned int width, unsigned int height, unsigned int psm);
    };

}}} // LightSource::Core::Graphics

#endif