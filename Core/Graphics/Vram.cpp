#include "Vram.h"

namespace LightSource { namespace Core { namespace Graphics {

	unsigned int VideoRam::m_staticOffset = 0;

	unsigned int VideoRam::GetMemorySize(unsigned int width, unsigned int height, unsigned int psm)
	{
		if (psm == GU_PSM_T4) return (width * height) >> 1;
		else if (psm == GU_PSM_T8) return width * height;
		else if ((psm == GU_PSM_5650) || (psm == GU_PSM_5551) || (psm == GU_PSM_4444) || (psm == GU_PSM_T16)) return 2 * width * height;
		else if ((psm == GU_PSM_8888) || (psm == GU_PSM_T32)) return 4 * width * height;
		else return 0;
	}

	void* VideoRam::GetAddress()
	{
		return (void*)(VideoRam::m_staticOffset + (unsigned int)sceGeEdramGetAddr());
	}


	void* VideoRam::AllocBuffer(unsigned int width, unsigned int height, unsigned int psm)
	{
		unsigned int memSize = VideoRam::GetMemorySize(width,height,psm);
		void* result = (void*)VideoRam::m_staticOffset;
		VideoRam::m_staticOffset += memSize;

		return result;
	}

	void* VideoRam::AllocTexture(unsigned int width, unsigned int height, unsigned int psm)
	{
		void* result = VideoRam::AllocBuffer(width,height,psm);
		return (void*)(((unsigned int)result) + ((unsigned int)sceGeEdramGetAddr()));
	}

}}} // LightSource::Core::Graphics