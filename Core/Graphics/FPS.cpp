#include "FPS.h"

namespace LightSource { namespace Core { namespace Graphics {

    FPS::FPS()
    {
        System::Timer::CurrentTick(&this->m_last_tick);

        this->m_tick_resolution = System::Timer::TickResolution();

        this->m_curr_ms = 1.0f;
        this->m_curr_fps = 1.0f;
    }

    FPS::~FPS()
    {

    }

    float FPS::GetLatency() const
    {
        return this->m_curr_ms;
    }

    float FPS::GetCurrentFPS() const
    {
        return this->m_curr_fps;
    }


    void FPS::PreUpdate()
    {
        ++this->m_frame_count;

        System::Timer::CurrentTick(&this->m_curr_tick);

        this->m_curr_fps = 1.0f / this->m_curr_ms;
    }

    void FPS::PostUpdate()
    {
        this->m_curr_ms = ((this->m_curr_tick - this->m_last_tick) / (float)this->m_tick_resolution) / this->m_frame_count;
        this->m_frame_count = 0;
        System::Timer::CurrentTick(&this->m_last_tick);
    }

}}} // LightSource::Core::Graphics