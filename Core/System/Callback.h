#ifndef LIGHT_SOURCE_CORE_SYSTEM_CALLBACK_H
#define LIGHT_SOURCE_CORE_SYSTEM_CALLBACK_H

#include "Thread.h"

#include <psploadexec.h>

namespace LightSource { namespace Core { namespace System {

    class Callback
    {
    private:
        Thread* m_thread;

    public:
        Callback();
        virtual ~Callback();
    };
    
}}} // LightSource::Core::System

/*
 |-------------------------------------------------------------------------
 | Function Pointers
 |------------------------------------------------------------
 |
 | This is specific section for function pointers, becouse
 | member function pointers can't used like a 
 | function pointers.
 |
 */

int light_source_core_system_callback_thread(SceSize args, void *argp);
int light_source_core_system_callback_exit(int arg1, int arg2, void *common);

#endif