#ifndef LIGHT_SOURCE_CORE_SYSTEM_MEMORY_H
#define LIGHT_SOURCE_CORE_SYSTEM_MEMORY_H

#include <pspsysmem.h>
#include <pspdebug.h>
#include <pspmoduleinfo.h>

#ifndef PSP_FAT
    #define PSP_HEAP_SIZE_KB_ALLOC_MAX 48 //48
#else
    #define PSP_HEAP_SIZE_KB_ALLOC_MAX 24 //24
#endif

namespace LightSource { namespace Core { namespace System {

    class Memory
    {
    private:

    public:
        Memory();
        ~Memory();

        SceUID Alloc(const char* name, SceSize size);
        SceUID Alloc(SceUID partitionid, const char *name, int type, SceSize size, void *addr);
        bool Free(SceUID blockid);

        void* GetAddress(SceUID blockid) const;

        SceSize GetTotalFreeSize() const;
        SceSize GetMaxFreeSize() const;

        /*
        |-------------------------------------------------------------------------
        | Enums
        |------------------------------------------------------------
        |
        | This is specific enums for Memory class.
        |
        */

        struct PartitionTypes
        {
            enum
            {
                PARTITION_ID_1 = 1,
                PARTITION_ID_2,
                PARTITION_ID_3,
                PARTITION_ID_4,
                PARTITION_ID_5,
                PARTITION_ID_6,
                PARTITION_ID_7,
                PARTITION_ID_8
            };
        };

        
        struct BlockTypes
        {
            enum
            {
                /** Allocate from the lowest available address. */
                PSP_SMEM_Low = 0,
                /** Allocate from the highest available address. */
                PSP_SMEM_High,
                /** Allocate from the specified address. */
                PSP_SMEM_Addr
            };
        };
    };

}}} // LightSource::Core::System

#endif