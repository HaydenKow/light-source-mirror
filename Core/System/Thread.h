#ifndef LIGHT_SOURCE_CORE_SYSTEM_THREAD_H
#define LIGHT_SOURCE_CORE_SYSTEM_THREAD_H

#include <pspthreadman.h>

namespace LightSource { namespace Core { namespace System {
            
    class Thread
    {
    private:
        int m_threadId;

    public:
        Thread(int threadId);
        virtual ~Thread();

        bool IsValid() const;

        int GetThreadId() const;
        
        int Start(unsigned int arglen, void* argp);

        /**
         * @brief Create a thread
         * 
         * @param name - An arbitrary thread name.
         * @param entry - The thread function to run when started.
         * @param initPriotity - The initial priority of the thread. Less if higher priority.
         * @param stackSize - The size of the initial stack.
         * @param attr - The thread attributes, zero or more of PspThreadAttributes.
         * @param option - Additional options specified by SceKernelThreadOptParam.
         * @return Thread* 
         */
        static Thread* Create(const char* name, SceKernelThreadEntry entry, int initPriotity, int stackSize, unsigned int attr, SceKernelThreadOptParam* option);
    };

}}} // LightSource::Core::System

#endif