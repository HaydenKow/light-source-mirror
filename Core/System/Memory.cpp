#include "Memory.h"

PSP_HEAP_SIZE_KB(-1024 * PSP_HEAP_SIZE_KB_ALLOC_MAX);

namespace LightSource { namespace Core { namespace System {

    Memory::Memory() {}
    Memory::~Memory() {}

    SceUID Memory::Alloc(const char* name, SceSize size)
    {
        return this->Alloc(PartitionTypes::PARTITION_ID_2, name, BlockTypes::PSP_SMEM_High, size, NULL);
    }

    SceUID Memory::Alloc(SceUID partitionid, const char *name, int type, SceSize size, void *addr)
    {
        return sceKernelAllocPartitionMemory(partitionid, name, type, size, addr);
    }

    bool Memory::Free(SceUID blockid)
    {
        return sceKernelFreePartitionMemory(blockid) >= 0;
    }

    void* Memory::GetAddress(SceUID blockid) const
    {
        return sceKernelGetBlockHeadAddr(blockid);
    }

    SceSize Memory::GetTotalFreeSize() const
    {
        return sceKernelTotalFreeMemSize();
    }

    SceSize Memory::GetMaxFreeSize() const
    {
        return sceKernelMaxFreeMemSize();
    }

}}} // LightSource::Core::System