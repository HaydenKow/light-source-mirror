#include "Thread.h"

namespace LightSource { namespace Core { namespace System {

    Thread::Thread(int threadId)
    {
        m_threadId = threadId;
    }

    Thread::~Thread()
    {
    }

    bool Thread::IsValid() const
    {
        return m_threadId >= 0;
    }

    int Thread::GetThreadId() const
    {
        return m_threadId;
    }

    int Thread::Start(unsigned int arglen, void* argp)
    {
        return sceKernelStartThread(m_threadId, arglen, argp);
    }

    Thread* Thread::Create(const char* name, SceKernelThreadEntry entry, int initPriotity, int stackSize, unsigned int attr, SceKernelThreadOptParam* option)
    {
        return new Thread(sceKernelCreateThread(name, entry, initPriotity, stackSize, attr, option));
    }

}}} // LightSource::Core::System