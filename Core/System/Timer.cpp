#include "Timer.h"

namespace LightSource { namespace Core { namespace System {

    u32 Timer::TickResolution()
    {
        return sceRtcGetTickResolution();
    }

    int Timer::CurrentTick(u64* tick)
    {
        return sceRtcGetCurrentTick(tick);
    }

}}} // LightSource::Core::System