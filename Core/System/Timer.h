#ifndef LIGHT_SOURCE_CORE_SYSTEM_TIMER_H
#define LIGHT_SOURCE_CORE_SYSTEM_TIMER_H

/*
 * D:/PSP/pspsdk/psp/sdk/include/psprtc.h:244: error: expected ',' or '...' before 'time'
 * D:/PSP/pspsdk/psp/sdk/include/psprtc.h:244: error: ISO C++ forbids declaration of 'time_t' with no type
 * D:/PSP/pspsdk/psp/sdk/include/psprtc.h:245: error: 'time_t' has not been declared
 */
typedef long time_t;

#include <psptypes.h>
#include <psprtc.h>

namespace LightSource { namespace Core { namespace System {
            
    class Timer
    {
    public:
        static u32 TickResolution();
        static int CurrentTick(u64* tick);
    };

}}} // LightSource::Core::System

#endif