#include "Callback.h"

namespace LightSource { namespace Core { namespace System {

    Callback::Callback()
    {
        m_thread = Thread::Create("update_thread", light_source_core_system_callback_thread, 0x11, 0xFA0, 0, 0);
        
        if (m_thread->IsValid())
        {
            m_thread->Start(0, 0);
        }
    }

    Callback::~Callback() {}

}}} // LightSource::Core::System

/*
 |-------------------------------------------------------------------------
 | Function Pointers
 |------------------------------------------------------------
 |
 | This is specific section for function pointers, becouse
 | member function pointers can't used like a 
 | function pointers.
 |
 */

int light_source_core_system_callback_thread(SceSize args, void *argp)
{
	int cbid = sceKernelCreateCallback("Exit Callback", light_source_core_system_callback_exit, NULL);
	sceKernelRegisterExitCallback(cbid);

	sceKernelSleepThreadCB();

	return 0;
}

int light_source_core_system_callback_exit(int arg1, int arg2, void *common)
{
    sceKernelExitGame();

    return 0;
}