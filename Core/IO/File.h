#ifndef LIGHT_SOURCE_CORE_IO_FILE_H
#define LIGHT_SOURCE_CORE_IO_FILE_H

#include <psptypes.h>
#include <pspiofilemgr.h>

namespace LightSource { namespace Core { namespace IO {
    
    class File
    {
    public:
        static SceUID Open(const char* file, int flags, SceMode mode);
        static SceUID OpenAsync(const char* file, int flags, SceMode mode);

        static bool Close(SceUID fileId);
        static bool CloseAsync(SceUID fileId);

        static int Read(SceUID fileId, void* buff, SceSize size);
        static int ReadAsync(SceUID fileId, void* buff, SceSize size);
    };

}}} // LightSource::Core::IO

#endif