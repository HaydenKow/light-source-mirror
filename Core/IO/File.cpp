#include "File.h"

namespace LightSource { namespace Core { namespace IO {

    SceUID File::Open(const char* file, int flags, SceMode mode)
    {
        return sceIoOpen(file, flags, mode);
    }

    SceUID File::OpenAsync(const char* file, int flags, SceMode mode)
    {
        return sceIoOpenAsync(file, flags, mode);
    }

    bool File::Close(SceUID fileId)
    {
        return sceIoClose(fileId) >= 0;
    }

    bool File::CloseAsync(SceUID fileId)
    {
        return sceIoCloseAsync(fileId) >= 0;
    }

    int File::Read(SceUID fileId, void* buff, SceSize size)
    {
        return sceIoRead(fileId, buff, size);
    }

    int File::ReadAsync(SceUID fileId, void* buff, SceSize size)
    {
        return sceIoReadAsync(fileId, buff, size);
    }



}}} // LightSource::Core::IO