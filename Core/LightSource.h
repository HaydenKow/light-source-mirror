#ifndef LIGHT_SOURCE_CORE_ENGINE
#define LIGHT_SOURCE_CORE_ENGINE

#include <pspkernel.h>

#include "_ECS_/API.h"
#include "_ECS_/Memory/GlobalMemoryUser.h"
#include "_ECS_/System/SystemManager.h"
#include "_ECS_/Entity/EntityManager.h"
#include "_ECS_/Components/ComponentManager.h"

#include "Graphics/Render.h"
#include "Graphics/Font.h"
#include "System/Callback.h"

namespace LightSource { namespace Core {

    class Engine
    {
    private:
        ECS::Memory::GlobalMemoryUser*              m_globalMemory;

        ECS::System::SystemManager*                 m_systemManager;
        ECS::Entity::EntityManager*                 m_entityManager;
        ECS::Components::ComponentManager*          m_componentManager;

        //--------------------------------------------------------------

        Graphics::Render*                           m_render;
        Graphics::Font*                             m_font;

        System::Callback*                           m_callback;

    public:
        Engine();
        ~Engine();

        ECS::System::SystemManager* GetSystemManager() const;
        ECS::Entity::EntityManager* GetEntityManager() const;

        Graphics::Render* GetRender() const;
        Graphics::Font* GetFont() const;
        
        void Dispose();
    };

}} // LightSource::Core

#endif