#ifndef LIGHT_SOURCE_CORE_CONTROLLER_CONTROLLER_INTERFACE_H
#define LIGHT_SOURCE_CORE_CONTROLLER_CONTROLLER_INTERFACE_H

#include <pspctrl.h>

namespace LightSource { namespace Core { namespace Controller {

    class IController
    {
    private:
        SceCtrlData m_pad;

    public:
        IController();
        virtual ~IController();

        void InputHandler();

        virtual void ButtonSelect();
        virtual void ButtonStart();
        virtual void ButtonUp();
        virtual void ButtonRight();
        virtual void ButtonDown();
        virtual void ButtonLeft();
        virtual void ButtonLeftTrigger();
        virtual void ButtonRightTrigger();
        virtual void ButtonTriangle();
        virtual void ButtonCircle();
        virtual void ButtonCross();
        virtual void ButtonSquare();
        virtual void ButtonHome();
        virtual void ButtonHold();
        virtual void ButtonNote();
        virtual void ButtonScreen();
        virtual void ButtonVolup();
        virtual void ButtonVoldown();
        virtual void ButtonWlanUp();
        virtual void ButtonRemote();
        virtual void ButtonDisc();
        virtual void ButtonMemoryStick();
        virtual void AnalogX(float x);
        virtual void AnalogY(float y);
    };

}}} // LightSource::Core::Controller

#endif