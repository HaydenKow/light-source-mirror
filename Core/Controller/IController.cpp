#include "IController.h"

namespace LightSource { namespace Core { namespace Controller {

    IController::IController()
    {
        sceCtrlSetSamplingCycle(0);
        sceCtrlSetSamplingMode(PSP_CTRL_MODE_ANALOG);
    }

    IController::~IController()
    {
    }

    void IController::InputHandler()
    {
        sceCtrlReadBufferPositive(&m_pad, 1);

        if (m_pad.Buttons != 0){
            if (m_pad.Buttons & PSP_CTRL_SELECT) ButtonSelect();
            if (m_pad.Buttons & PSP_CTRL_START) ButtonStart();
            if (m_pad.Buttons & PSP_CTRL_UP) ButtonUp();
            if (m_pad.Buttons & PSP_CTRL_RIGHT) ButtonRight();
            if (m_pad.Buttons & PSP_CTRL_DOWN) ButtonDown();
            if (m_pad.Buttons & PSP_CTRL_LEFT) ButtonLeft();
            if (m_pad.Buttons & PSP_CTRL_LTRIGGER) ButtonLeftTrigger();
            if (m_pad.Buttons & PSP_CTRL_RTRIGGER) ButtonRightTrigger();
            if (m_pad.Buttons & PSP_CTRL_TRIANGLE) ButtonTriangle();
            if (m_pad.Buttons & PSP_CTRL_CIRCLE) ButtonCircle();
            if (m_pad.Buttons & PSP_CTRL_CROSS) ButtonCross();
            if (m_pad.Buttons & PSP_CTRL_SQUARE) ButtonSquare();
            if (m_pad.Buttons & PSP_CTRL_HOME) ButtonHome();
            if (m_pad.Buttons & PSP_CTRL_HOLD) ButtonHold();
            if (m_pad.Buttons & PSP_CTRL_NOTE) ButtonNote();
            if (m_pad.Buttons & PSP_CTRL_SCREEN) ButtonScreen();
            if (m_pad.Buttons & PSP_CTRL_VOLUP) ButtonVolup();
            if (m_pad.Buttons & PSP_CTRL_VOLDOWN) ButtonVoldown();
            if (m_pad.Buttons & PSP_CTRL_WLAN_UP) ButtonWlanUp();
            if (m_pad.Buttons & PSP_CTRL_REMOTE) ButtonRemote();
            if (m_pad.Buttons & PSP_CTRL_DISC) ButtonDisc();
            if (m_pad.Buttons & PSP_CTRL_MS) ButtonMemoryStick();
        }

        AnalogX(m_pad.Lx);
        AnalogY(m_pad.Ly);
    }

    void IController::ButtonSelect(){}
    void IController::ButtonStart(){}
    void IController::ButtonUp(){}
    void IController::ButtonRight(){}
    void IController::ButtonDown(){}
    void IController::ButtonLeft(){}
    void IController::ButtonLeftTrigger(){}
    void IController::ButtonRightTrigger(){}
    void IController::ButtonTriangle(){}
    void IController::ButtonCircle(){}
    void IController::ButtonCross(){}
    void IController::ButtonSquare(){}
    void IController::ButtonHome(){}
    void IController::ButtonHold(){}
    void IController::ButtonNote(){}
    void IController::ButtonScreen(){}
    void IController::ButtonVolup(){}
    void IController::ButtonVoldown(){}
    void IController::ButtonWlanUp(){}
    void IController::ButtonRemote(){}
    void IController::ButtonDisc(){}
    void IController::ButtonMemoryStick(){}
    void IController::AnalogX(float x){}
    void IController::AnalogY(float y){}

}}} // LightSource::Core::Controller