#include "LightSource.h"

namespace LightSource { namespace Core {

    Engine::Engine()
    {
        this->m_globalMemory = new ECS::Memory::GlobalMemoryUser();
        this->m_systemManager = new ECS::System::SystemManager(m_globalMemory);
        this->m_componentManager = new ECS::Components::ComponentManager(m_globalMemory);
        this->m_entityManager = new ECS::Entity::EntityManager(m_globalMemory, m_componentManager);

        this->m_render = new Graphics::Render();
        this->m_font = new Graphics::Font("flash0:/font/ltn8.pgf", 0);
        this->m_callback = new System::Callback();

        sceKernelDcacheWritebackAll();
    }

    Engine::~Engine()
    {
        this->Dispose();
    }

    ECS::System::SystemManager* Engine::GetSystemManager() const
    {
        return this->m_systemManager;
    }

    ECS::Entity::EntityManager* Engine::GetEntityManager() const
    {
        return this->m_entityManager;
    }

    Graphics::Render* Engine::GetRender() const
    {
        return this->m_render;
    }

    Graphics::Font* Engine::GetFont() const
    {
        return this->m_font;
    }

    void Engine::Dispose()
    {
        this->m_render->Dispose();
    }

}} // LightSource::Core