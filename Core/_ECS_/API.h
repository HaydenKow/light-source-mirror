#ifndef LIGTH_SOURCE_CORE_ECS_API_H
#define LIGTH_SOURCE_CORE_ECS_API_H

#include <pspdebug.h>
#include <psptypes.h>

#include <algorithm>
#include <vector>
#include <map>
#include <unordered_map>

#include <cstdlib>

/*
 |----------------------------------------------------------------------------------------------------------
 | Memory
 |------------------------------------------------------------------------------------------
 |
 | All available memory is allocated at a time.
 |
*/

#define SYSTEM_MEMORY_OFFSET            0                                                       // start on firstAddress
#define SYSTEM_MEMORY_BUFFER_SIZE       1048576                                                 // 1MB

#define ENTITY_MEMORY_OFFSET            (SYSTEM_MEMORY_OFFSET + SYSTEM_MEMORY_BUFFER_SIZE)      // start after end of system
#define ENTITY_MEMORY_BUFFER_SIZE       1048576                                                 // 1MB

#define COMPONENT_MEMORY_OFFSET         (ENTITY_MEMORY_OFFSET + ENTITY_MEMORY_BUFFER_SIZE)      // start after end of entity

/*
 |----------------------------------------------------------------------------------------------------------
 | Types
 |------------------------------------------------------------------------------------------
 |
 | 
 |
*/

/* ECS */
typedef SceUInt32 EntityId;
typedef SceUInt32 ComponentId;
typedef SceUInt32 HashId;

/* Vectors */
typedef struct Vector32BITF {
    float u, v;
    float nx, ny, nz;
    float x,y,z;
} Vector32BITF;

/* Textures */
typedef struct TextureRGBA {
    unsigned char r, g, b, a;
} TextureRGBA;

/*
 |----------------------------------------------------------------------------------------------------------
 | The Structure Of The Entity Component System
 |------------------------------------------------------------------------------------------
 |
 | 
 |
*/

namespace LightSource { namespace Core { namespace ECS {

    namespace System
    {
        class ISystem;

        template<class T>
        class SystemFamily;

        class SystemManager;
    }

    namespace Entity
    {

        /*
        |-----------------------------------------------------------------------------------
        | Note:
        |------------------------------------------------------------------------------
        |
        | EntityId is responsible for the unique identifier of all entities regardless 
        | of the class type. Therefore the EntityManager is a conventional counter 
        | to assign the EntityId and why has't EntityFamily. 
        | There's no reason to do it.
        |
        */

        class IEntity;
        class EntityManager;
    }

    namespace Components
    {
        class IComponent;
        
        template<class T>
        class ComponentFamily;

        class ComponentManager;
    }

    namespace Memory
    {
        class IAllocator;
        class LinearAllocator;
        class GlobalMemoryUser;
    }

    namespace Utils
    {
        template<class T>
        class FamilyTypeID;
    }

}}} // LightSource::Core::ECS

#endif