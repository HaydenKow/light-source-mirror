#ifndef LIGHT_SOURCE_CORE_ECS_SYSTEM_SYSTEM_MANAGER_H
#define LIGHT_SOURCE_CORE_ECS_SYSTEM_SYSTEM_MANAGER_H

#include <pspkerneltypes.h>
#include <pspdebug.h>

#include <algorithm>
#include <vector>

#include "../API.h"
#include "../Memory/GlobalMemoryUser.h"
#include "../Memory/LinearAllocator.h"
#include "ISystem.h"

namespace LightSource { namespace Core { namespace ECS { namespace System {

    class SystemManager
    {
    private:
        Memory::LinearAllocator*            m_systemAllocator;
        std::vector<ISystem*>               m_systems;

    public:
        SystemManager(Memory::GlobalMemoryUser* globalMemory);
        ~SystemManager();

        template<class T, class... Args>
        T* AddSystem(Args... systemArgs)
        {
            //check to avoid multiple registrations of the same system
            const SceUInt32 STID = T::STATIC_SYSTEM_TYPE_ID;

            T* system = NULL;
            void* pSystemMem = this->Find(STID);

            // if same system not exists then allocate memory
            if (pSystemMem == NULL) {
                pSystemMem = this->m_systemAllocator->Allocate(sizeof(T), sizeof(T));

                // add a new system
                if (pSystemMem != NULL) {
                    system = new (pSystemMem)T(std::forward<Args>(systemArgs)...);
                    system->SetSTID(STID);
                    m_systems.push_back(system);
                }
            } else {
                // else system is exists then apply a pointer
                system = new (pSystemMem)T(std::forward<Args>(systemArgs)...);
            }

            return system;
        }

        ISystem* Find(const SceUInt32 STID) const
        {
            for (unsigned int i = 0; i < m_systems.size(); i++) {
                if (STID == m_systems[i]->GetSTID()) {
                    return m_systems[i];
                }
            }

            return NULL;
        }

        std::vector<ISystem*> GetSystems() const;

        void Sort();
        void Update();
    };

}}}} // LightSource::Core::ECS::System

#endif