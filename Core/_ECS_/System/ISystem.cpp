#include "ISystem.h"

namespace LightSource { namespace Core { namespace ECS { namespace System {

    ISystem::ISystem()
    {
    }

    ISystem::~ISystem()
    {
    }

    bool ISystem::IsEnabled() const
    {
        return m_enabled;
    }

    SceUShort16 ISystem::GetSTID() const
    {
        return m_STID;
    }

    SceUShort16 ISystem::GetPriority() const
    {
        return m_priority;
    }

    SceFloat ISystem::GetTimeSinceLastUpdate() const
    {
        return m_timeSinceLastUpdate;
    }

    void ISystem::SetEnabled(bool enabled)
    {
        m_enabled = enabled;
    }

    void ISystem::SetSTID(SceUShort16 STID)
    {
        m_STID = STID;
    }

    void ISystem::SetPriority(SceUShort16 priority)
    {
        m_priority = priority;
    }

    void ISystem::SetTimeSinceLastUpdate(SceFloat timer)
    {
        m_timeSinceLastUpdate = timer;
    }


}}}} // LightSource::Core::ECS::System