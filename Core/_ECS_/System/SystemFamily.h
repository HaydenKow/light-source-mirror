#ifndef LIGHT_SOURCE_CORE_ECS_SYSTEM_SYSTEM_FAMILY_H
#define LIGHT_SOURCE_CORE_ECS_SYSTEM_SYSTEM_FAMILY_H

#include <psptypes.h>

#include "ISystem.h"
#include "../Utils/FamilyTypeID.h"

namespace LightSource { namespace Core { namespace ECS { namespace System {

    template<class T>
    class SystemFamily
    {
    public:
        static const SceUInt32 STATIC_SYSTEM_TYPE_ID;
    };
    
    template<class T>
    const SceUInt32 SystemFamily<T>::STATIC_SYSTEM_TYPE_ID = Utils::FamilyTypeID<ISystem>::Get<T>();

}}}} // LightSource::Core::ECS::System

#endif