#include "SystemManager.h"

namespace LightSource { namespace Core { namespace ECS { namespace System {

    SystemManager::SystemManager(Memory::GlobalMemoryUser* globalMemory)
    {
        m_systemAllocator = new Memory::LinearAllocator(SYSTEM_MEMORY_BUFFER_SIZE, globalMemory->GetOffsetAddress(SYSTEM_MEMORY_OFFSET));
    }

    SystemManager::~SystemManager()
    {
        m_systems.clear();
    }

    std::vector<ISystem*> SystemManager::GetSystems() const
    {
        return m_systems;
    }

    void SystemManager::Sort()
    {
        std::sort(m_systems.begin(), m_systems.end(), ISystemSortPriorityComparison());
    }

    void SystemManager::Update()
    {
        unsigned int i;

        // PreUpdate
        for(i = 0; i < m_systems.size(); i++) m_systems[i]->PreUpdate();
        // Update
        for(i = 0; i < m_systems.size(); i++) m_systems[i]->Update();
        // PostUpdate
        for(i = 0; i < m_systems.size(); i++) m_systems[i]->PostUpdate();
    }

}}}} // LightSource::Core::ECS::System