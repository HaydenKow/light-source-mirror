#ifndef LIGHT_SOURCE_CORE_ECS_SYSTEM_INTERFACE_SYSTEM_H
#define LIGHT_SOURCE_CORE_ECS_SYSTEM_INTERFACE_SYSTEM_H

#include <psptypes.h>

namespace LightSource { namespace Core { namespace ECS { namespace System {

    class ISystem
    {
    private:
        bool                m_enabled;
        SceUShort16         m_STID;                         // STATIC_SYSTEM_TYPE_ID
        SceUShort16         m_priority;                     // PRIORITY (above has more priority)
        SceFloat            m_timeSinceLastUpdate;

    public:
        ISystem();
        virtual ~ISystem();

        bool IsEnabled() const;
        SceUShort16 GetSTID() const;
        SceUShort16 GetPriority() const;
        SceFloat GetTimeSinceLastUpdate() const;

        void SetEnabled(bool enabled);
        void SetSTID(SceUShort16 STID);
        void SetPriority(SceUShort16 priority);
        void SetTimeSinceLastUpdate(SceFloat timer);

        virtual void PreUpdate() = 0;
        virtual void Update() = 0;
        virtual void PostUpdate() = 0;

        inline bool operator== (const ISystem &rhs) const
        {
            return this->GetSTID() == rhs.GetSTID();
        }
    };

    struct ISystemSortPriorityComparison
    {
        bool const operator()(ISystem *lhs, ISystem *rhs) const
        {
            return (*lhs).GetPriority() > (*rhs).GetPriority();
        }
    };

}}}} // LightSource::Core::ECS::System

#endif