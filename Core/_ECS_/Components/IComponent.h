#ifndef LIGHT_SOURCE_CORE_ECS_COMPONENTS_INTERFACE_COMPONENT_H
#define LIGHT_SOURCE_CORE_ECS_COMPONENTS_INTERFACE_COMPONENT_H

#include "../API.h"

namespace LightSource { namespace Core { namespace ECS { namespace Components {
    
    class IComponent
    {
    protected:
        bool                    m_enabled;
        HashId                  m_hash;
        ComponentId             m_componentId;
        EntityId                m_entityId;

    public:
        IComponent();
        virtual ~IComponent();

        inline const HashId GetHash() const { return m_hash; }
        inline const ComponentId GetComponentId() const { return m_componentId; }
        inline const EntityId GetEntityId() const { return m_entityId; }

        inline const void SetHash(const HashId hash) { m_hash = hash; }
        inline const void SetComponentId(const ComponentId componentId) { m_componentId = componentId; }
        inline const void SetEntityId(const EntityId entityId) { m_entityId = entityId; }

        inline const bool operator==(const IComponent& other) const { return m_hash == other.m_hash; }
        inline const bool operator!=(const IComponent& other) const { return m_hash != other.m_hash; }

        inline const bool operator==(const IComponent* other) const { return m_hash == other->m_hash; }
        inline const bool operator!=(const IComponent* other) const { return m_hash != other->m_hash; }
    };

}}}} // LightSource::Core::ECS::Components

#endif