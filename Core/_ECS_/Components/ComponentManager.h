#ifndef LIGHT_SOURCE_CORE_ECS_COMPONENTS_COMPONENT_MANAGER_H
#define LIGHT_SOURCE_CORE_ECS_COMPONENTS_COMPONENT_MANAGER_H

#include <pspdebug.h>

#include "../API.h"
#include "../Memory/GlobalMemoryUser.h"
#include "../Memory/LinearAllocator.h"

#include "IComponent.h"

namespace LightSource { namespace Core { namespace ECS { namespace Components {

    class ComponentManager
    {
    private:
        Memory::LinearAllocator* m_componentAllocator;
        std::unordered_map<EntityId, std::unordered_map<ComponentId, IComponent*>> m_entityComponentMap;

    public:
        ComponentManager(Memory::GlobalMemoryUser* globalMemory);
        ~ComponentManager();

        template<class T, class... Args>
        T* AddComponent(const EntityId entityId, Args... componentArgs)
        {
            T* component = NULL;
            void* pComponentMem = NULL;

            const ComponentId CTID = T::STATIC_COMPONENT_TYPE_ID;

            // avoid multiply same components to one entity
            if (this->m_entityComponentMap.find(entityId) != this->m_entityComponentMap.end()) {
                pComponentMem = (void*)this->Find(entityId, CTID);
                
                if (pComponentMem != NULL) {
                    // component already exists in current entityId
                    component = new (pComponentMem)T(std::forward<Args>(componentArgs)...);

                    return component;
                }
            }

            // create a new component in current entityId
            pComponentMem = this->m_componentAllocator->Allocate(sizeof(T), sizeof(T));
            if (pComponentMem != NULL) {
                ((IComponent*)pComponentMem)->SetComponentId(CTID);
                ((IComponent*)pComponentMem)->SetEntityId(entityId);

                component = new (pComponentMem)T(std::forward<Args>(componentArgs)...);

                m_entityComponentMap[entityId][CTID] = component;
            }

            return component;
        }

        inline IComponent* Find(const EntityId entityId, const ComponentId componentId)
        {
            for (std::unordered_map<ComponentId, IComponent*>::const_iterator it = this->m_entityComponentMap[entityId].begin(); it != this->m_entityComponentMap[entityId].end(); ++it) {
                if (it->second->GetComponentId() == componentId) {
                    return it->second;
                }
            }

            return NULL;
        }

        template<class T>
        T* GetComponent(const EntityId entityId)
        {
            return (T*)this->Find(entityId, T::STATIC_COMPONENT_TYPE_ID);
        }
    };

}}}} // LightSource::Core::ECS::Components

#endif