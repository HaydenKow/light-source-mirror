#include "ComponentManager.h"

namespace LightSource { namespace Core { namespace ECS { namespace Components {

    ComponentManager::ComponentManager(Memory::GlobalMemoryUser* globalMemory)
    {
        // TODO: calculate the remaining amount of memory 
        m_componentAllocator = new Memory::LinearAllocator(20*1024*1024, globalMemory->GetOffsetAddress(COMPONENT_MEMORY_OFFSET));
    }

    ComponentManager::~ComponentManager()
    {
        m_entityComponentMap.clear();
    }

}}}} // LightSource::Core::ECS::Components