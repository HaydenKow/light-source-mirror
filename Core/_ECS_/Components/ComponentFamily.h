#ifndef LIGHT_SOURCE_CORE_ECS_COMPONENTS_COMPONENT_FAMILY_H
#define LIGHT_SOURCE_CORE_ECS_COMPONENTS_COMPONENT_FAMILY_H

#include "../API.h"
#include "../Utils/FamilyTypeID.h"
#include "IComponent.h"

namespace LightSource { namespace Core { namespace ECS { namespace Components {
    
    template<class T>
    class ComponentFamily
    {
    public:
        static const SceUInt32 STATIC_COMPONENT_TYPE_ID;
    };

    template<class T>
    const SceUInt32 ComponentFamily<T>::STATIC_COMPONENT_TYPE_ID = Utils::FamilyTypeID<IComponent>::Get<T>();

}}}} // LightSource::Core::ECS::Components

#endif