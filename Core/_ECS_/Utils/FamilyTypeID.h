#ifndef LIGHT_SOURCE_CORE_ECS_UTILS_FAMILY_TYPE_ID_H
#define LIGHT_SOURCE_CORE_ECS_UTILS_FAMILY_TYPE_ID_H

#include "../API.h"

namespace LightSource { namespace Core { namespace ECS { namespace Utils {

    template<class T>
    class FamilyTypeID
    {
    protected:
        static SceUInt32 m_count;

    public:
        template<class U>
        static const SceUInt32 Get()
        {
            static const SceUInt32 STATIC_TYPE_ID = m_count++;
            return STATIC_TYPE_ID;
        }

        static const SceUInt32 Get()
        {
            return m_count;
        }
    };

    template<class T>
    SceUInt32 FamilyTypeID<T>::m_count = 1;

}}}} // LightSource::Core::ECS::Utils

#endif