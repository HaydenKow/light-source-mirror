#include "IEntity.h"

namespace LightSource { namespace Core { namespace ECS { namespace Entity {

    IEntity::IEntity() {}

    IEntity::~IEntity() {}

    void IEntity::SetActive(bool active)
    {
        m_active = active;
    }

    void IEntity::SetEntityID(EntityId entityId)
    {
        m_entityId = entityId;
    }

    void IEntity::SetComponentManager(Components::ComponentManager* componentManagerInstance)
    {
        m_componentManagerInstance = componentManagerInstance;
    }

}}}} // LightSource::Core::ECS::Entity