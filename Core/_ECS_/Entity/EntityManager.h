#ifndef LIGHT_SOURCE_CORE_ECS_ENTITY_ENTITY_MANAGER_H
#define LIGHT_SOURCE_CORE_ECS_ENTITY_ENTITY_MANAGER_H

#include <pspdebug.h>

#include "../API.h"
#include "../Components/ComponentManager.h"
#include "../Memory/GlobalMemoryUser.h"
#include "../Memory/LinearAllocator.h"
#include "IEntity.h"

#define ENTITY_COUNTER_START 1

namespace LightSource { namespace Core { namespace ECS { namespace Entity {

    class EntityManager
    {
    private:
        /*
        |-----------------------------------------------------------------------------------
        | Note:
        |------------------------------------------------------------------------------
        |
        | EntityId is responsible for the unique identifier of all entities regardless 
        | of the class type. Therefore the EntityManager is a conventional counter 
        | to assign the EntityId and why has't EntityFamily. 
        | There's no reason to do it.
        |
        */

        EntityId                                        m_counter;
        Memory::LinearAllocator*                        m_entityAllocator;
        std::unordered_map<EntityId, IEntity*>          m_entityTable;

        Components::ComponentManager*                   m_componentManagerInstance;

    public:
        EntityManager(Memory::GlobalMemoryUser* globalMemory, Components::ComponentManager* componentManagerInstance);
        ~EntityManager();

        template<class T, class... Args>
        EntityId CreateEntity(Args... entityArgs)
        {
            T* entity = NULL;
            void* pEntityMem = this->m_entityAllocator->Allocate(sizeof(T), sizeof(T));

            if (pEntityMem != NULL) {
                const EntityId entityId = m_counter++;

                ((IEntity*)pEntityMem)->SetEntityID(entityId);
                ((IEntity*)pEntityMem)->SetComponentManager(m_componentManagerInstance);

                entity = new (pEntityMem)T(std::forward<Args>(entityArgs)...);

                m_entityTable[entityId] = entity;

                return entityId;
            }

            return 0;
        }

        template<class T>
        inline T* GetEntity(EntityId entityId)
        {
            return (T*)this->m_entityTable[entityId];
        }

        inline IEntity* GetEntity(EntityId entityId)
        {
            return this->m_entityTable[entityId];
        }

        inline std::unordered_map<EntityId, IEntity*> GetEntitiesTable() const
        {
            return this->m_entityTable;
        }
    };

}}}} // LightSource::Core::ECS::Entity

#endif