#include "EntityManager.h"

namespace LightSource { namespace Core { namespace ECS { namespace Entity {

    EntityManager::EntityManager(Memory::GlobalMemoryUser* globalMemory, Components::ComponentManager* componentManagerInstance)
    {
        m_counter = ENTITY_COUNTER_START;
        m_entityAllocator = new Memory::LinearAllocator(ENTITY_MEMORY_BUFFER_SIZE, globalMemory->GetOffsetAddress(ENTITY_MEMORY_OFFSET));
        m_componentManagerInstance = componentManagerInstance;
    }

    EntityManager::~EntityManager()
    {
    }

}}}} // LightSource::Core::ECS::Entity