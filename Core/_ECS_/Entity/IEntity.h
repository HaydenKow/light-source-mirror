#ifndef LIGHT_SOURCE_CORE_ECS_ENTITY_INTERFACE_ENTITY_H
#define LIGHT_SOURCE_CORE_ECS_ENTITY_INTERFACE_ENTITY_H

#include "../API.h"
#include "../Components/ComponentManager.h"

namespace LightSource { namespace Core { namespace ECS { namespace Entity {

    class IEntity
    {
    private:
        bool                                    m_active;
        EntityId                                m_entityId;
        Components::ComponentManager*           m_componentManagerInstance;

    public:
        IEntity();
        virtual ~IEntity();

        template<class T, class... Args>
        T* AddComponent(Args... componentArgs)
        {
            return this->m_componentManagerInstance->AddComponent<T>(m_entityId, std::forward<Args>(componentArgs)...);
        }

        template<class T>
        T* GetComponent()
        {
            return this->m_componentManagerInstance->GetComponent<T>(m_entityId);
        }

        inline const Components::ComponentManager* GetComponentManager() const
        {
            return this->m_componentManagerInstance;
        }

        inline bool IsActive() const { return this->m_active; }
        inline const EntityId GetEntityID() const { return this->m_entityId; }

        void SetActive(bool active);
        void SetEntityID(EntityId entityId);
        void SetComponentManager(Components::ComponentManager* componentManagerInstance);

        inline bool operator==(const IEntity& rhs) const { return this->m_entityId == rhs.m_entityId; }
		inline bool operator!=(const IEntity& rhs) const { return this->m_entityId != rhs.m_entityId; }
		inline bool operator==(const IEntity* rhs) const { return this->m_entityId == rhs->m_entityId; }
		inline bool operator!=(const IEntity* rhs) const { return this->m_entityId != rhs->m_entityId; }
    };

}}}} // LightSource::Core::ECS::Entity

#endif