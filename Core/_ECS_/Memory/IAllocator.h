#ifndef LIGHT_SOURCE_CORE_ECS_MEMORY_INTERFACE_ALLOCATOR_H
#define LIGHT_SOURCE_CORE_ECS_MEMORY_INTERFACE_ALLOCATOR_H

#include <pspkerneltypes.h>

namespace LightSource { namespace Core { namespace ECS { namespace Memory {

    static inline uint8_t GetAdjustment(const void* address, uint8_t alignment)
	{
        // заменить unitptr_t на любое иное, но не 64 битное
		uint8_t adjustment = alignment - (reinterpret_cast<uintptr_t>(address)& static_cast<uintptr_t>(alignment - 1));
	
		return adjustment == alignment ? 0 : adjustment;
	}

    class IAllocator
    {
    protected:
        const SceSize           m_memorySize;
        const void*             m_memoryFirstAddress;

        SceUInt                 m_memoryUsed;
        SceUInt                 m_memoryAllocations;

    public:
        IAllocator(const SceUInt memSize, const void* mem);
        virtual ~IAllocator();

        virtual void* Allocate(SceSize size, uint8_t alignment) = 0;
        virtual void Free(void* pointer) = 0;
        virtual void Clear() = 0;

        inline SceSize GetMemorySize() const { return this->m_memorySize; }
        inline const void* GetMemoryBegunAddress() const { return this->m_memoryFirstAddress; }
        inline SceUInt GetUsedMemory() const { return this->m_memoryUsed; }
        inline SceUInt GetAllocationCount() const { return this->m_memoryAllocations; }
    };

}}}} // LightSource::Core::ECS::Memory

#endif