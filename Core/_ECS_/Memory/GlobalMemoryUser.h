#ifndef LIGTH_SOURCE_CORE_ECS_MEMORY_GLOBAL_MEMORY_USER_H
#define LIGTH_SOURCE_CORE_ECS_MEMORY_GLOBAL_MEMORY_USER_H

#include "../../System/Memory.h"

namespace LightSource { namespace Core { namespace ECS { namespace Memory {

    class GlobalMemoryUser
    {
    private:
        LightSource::Core::System::Memory*      m_memory;
        int                                     m_blockId;

    public:
        GlobalMemoryUser();
        virtual ~GlobalMemoryUser();

        void* GetFirstAddress() const;
        void* GetOffsetAddress(int offset);
    };

}}}} // LightSource::Core::ECS::Memory

#endif