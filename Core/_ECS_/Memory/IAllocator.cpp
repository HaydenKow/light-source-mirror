#include "IAllocator.h"

namespace LightSource { namespace Core { namespace ECS { namespace Memory {

    IAllocator::IAllocator(const SceUInt memSize, const void* mem) :
        m_memorySize(memSize),
        m_memoryFirstAddress(mem),
        m_memoryUsed(0),
        m_memoryAllocations(0)
    {}

    IAllocator::~IAllocator()
    {}

}}}} // LightSource::Core::ECS::Memory