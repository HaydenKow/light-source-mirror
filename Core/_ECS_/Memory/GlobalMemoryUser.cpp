#include "GlobalMemoryUser.h"

namespace LightSource { namespace Core { namespace ECS { namespace Memory {

    GlobalMemoryUser::GlobalMemoryUser()
    {
        m_memory = new LightSource::Core::System::Memory();
        m_blockId = m_memory->Alloc("LigthSource::ECS::Memory::GlobalMemoryUser", m_memory->GetMaxFreeSize());
    }

    GlobalMemoryUser::~GlobalMemoryUser()
    {
        m_memory->Free(m_blockId);
    }

    void* GlobalMemoryUser::GetFirstAddress() const
    {
        return m_memory->GetAddress(m_blockId);
    }

    void* GlobalMemoryUser::GetOffsetAddress(int offset)
    {
        return (void*)((int)this->GetFirstAddress() + offset);
    }

}}}} // LightSource::Core::ECS::Memory