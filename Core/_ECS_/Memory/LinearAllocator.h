#ifndef LIGHT_SOURCE_CORE_ECS_MEMORY_LINEAR_ALLOCATOR_H
#define LIGHT_SOURCE_CORE_ECS_MEMORY_LINEAR_ALLOCATOR_H

#include "IAllocator.h"

namespace LightSource { namespace Core { namespace ECS { namespace Memory {

    /*
		Allocates memory in a linear way. 

		      first          2   3   4
		    allocatation     alloaction
		        v            v   v   v
		|=================|=====|==|======| .... |
		^                                        ^
		Initialial                               Last possible
		memory                                   memory address
		address                                  (mem + memSize)
		(mem)


		memory only can be freed by clearing all allocations
	*/
    class LinearAllocator : public IAllocator
    {
    public:
        LinearAllocator(const SceUInt memSize, const void* mem);
        virtual ~LinearAllocator();

        virtual void* Allocate(SceSize memSize, uint8_t alignment);
        virtual void Free(void* pointer);
        virtual void Clear();
    };

}}}} // LightSource::Core::ECS::Memory

#endif