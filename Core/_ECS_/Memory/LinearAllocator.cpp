#include "LinearAllocator.h"

namespace LightSource { namespace Core { namespace ECS { namespace Memory {

    LinearAllocator::LinearAllocator(const SceUInt memSize, const void* mem) :
        IAllocator(memSize, mem)
    {}

    LinearAllocator::~LinearAllocator()
    {
        this->Clear();
    }
    
    void* LinearAllocator::Allocate(SceSize memSize, uint8_t alignment)
    {
        //assert(memSize > 0 && "LinearAllocator::Allocate called with memory size = 0.");

        union
		{
			void* asVoidPtr;
			uintptr_t asUptr;
		};

		asVoidPtr = (void*)this->m_memoryFirstAddress;

		// current address
		asUptr += this->m_memoryUsed;

		// get adjustment to align address
		uint8_t adjustment = GetAdjustment(asVoidPtr, alignment);

		// check if there is enough memory available
		if (this->m_memoryUsed + memSize + adjustment > this->m_memorySize)
		{
			// not enough memory
			return NULL;
		}

		// determine aligned memory address
		asUptr += adjustment;

		// update book keeping
		this->m_memoryUsed += memSize + adjustment;
		this->m_memoryAllocations++;

		// return aligned memory address
		return asVoidPtr;
    }

    void LinearAllocator::Free(void* mem)
	{
		//assert(false && "Lineaer allocators do not support free operations. Use clear instead.");
	}

    void LinearAllocator::Clear()
	{
		// simply reset memory
		this->m_memoryUsed = 0;
		this->m_memoryAllocations = 0;
	}

}}}} // LightSource::Core::ECS::Memory