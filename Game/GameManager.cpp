#include "GameManager.h"

namespace LightSource { namespace Game {

    GameManager::GameManager(const Core::Engine* engine)
    {
        this->m_engineInstance = engine;

        /*
        |-----------------------------------------------------------------------------------
        | Systems
        |------------------------------------------------------------------------------
        |
        |
        |
        */

        this->m_renderSystem = this->m_engineInstance->GetSystemManager()->AddSystem<LightSource::Game::System::RenderSystem>(engine);
        
        //this->m_engineInstance->GetSystemManager()->AddSystem<LightSource::Game::System::ControllerSystem>();

        /*
        |-----------------------------------------------------------------------------------
        | Entities
        |------------------------------------------------------------------------------
        |
        | 1231xCube: that object count to draw has GTA Vice City Stores...
        |
        */

        //this->m_engineInstance->GetEntityManager()->CreateEntity<Entity::PlayerEntity>();

        for (unsigned int count = 0; count < 1; count++) {
            this->m_renderSystem->Add(this->m_engineInstance->GetEntityManager()->CreateEntity<Entity::PlayerEntity>());
        }
    }

    GameManager::~GameManager()
    {
    }

}} // LightSource::Game