#ifndef LIGHT_SOURCE_GAME_SYSTEM_CONTROLLER_SYSTEM_H
#define LIGHT_SOURCE_GAME_SYSTEM_CONTROLLER_SYSTEM_H

#include <pspdebug.h>
#include <pspctrl.h>

#include "../Core/_ECS_/System/SystemFamily.h"
#include "../Core/_ECS_/System/ISystem.h"

namespace LightSource { namespace Game { namespace System {

    class ControllerSystem : public Core::ECS::System::SystemFamily<ControllerSystem>, public Core::ECS::System::ISystem
    {
    private:
        SceCtrlData m_pad;
        
    public:
        ControllerSystem();
        ~ControllerSystem();

        virtual void PreUpdate();
        virtual void Update();
        virtual void PostUpdate();
    };

}}} // LightSource::Game::System

#endif