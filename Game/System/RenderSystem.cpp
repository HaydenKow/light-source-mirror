#include "RenderSystem.h"

namespace LightSource { namespace Game { namespace System {

    RenderSystem::RenderSystem(const Core::Engine* engine)
    {
        this->m_engineInstance = engine;
        this->m_engineInstance->GetRender()->Init();
        this->val = 0;
    }

    RenderSystem::~RenderSystem()
    {

    }

    std::vector<EntityId>::const_iterator RenderSystem::Find(const EntityId entityId)
    {
        return std::find(this->m_renderableEntityList.begin(), this->m_renderableEntityList.end(), entityId);
    }

    bool RenderSystem::Exists(const EntityId entityId)
    {
        return this->m_renderableEntityList.end() != this->Find(entityId);
    }

    void RenderSystem::Add(const EntityId entityId)
    {
        if (!this->Exists(entityId)) {
            this->m_renderableEntityList.push_back(entityId);
        }
    }

    void RenderSystem::Clear()
    {
        this->m_renderableEntityList.clear();
    }

    void RenderSystem::PreUpdate()
    {
        //this->m_engineInstance->GetRender()->Display()->DebugClear();
        
        this->m_engineInstance->GetRender()->FrameStart();
        this->m_engineInstance->GetRender()->FrameClear();

        // FPS
        this->m_engineInstance->GetRender()->FPS()->PreUpdate();
    }

    void RenderSystem::Update()
    {
        // setup matrices for cube
        sceGumMatrixMode(GU_PROJECTION);
        sceGumLoadIdentity();
        sceGumPerspective(75.0f,16.0f/9.0f,0.5f,1000.0f);

        sceGumMatrixMode(GU_VIEW);
        sceGumLoadIdentity();

        for (this->m_renderableIndex = 0; this->m_renderableIndex < this->m_renderableEntityList.size(); this->m_renderableIndex++) {
            if (this->m_renderableEntityList[this->m_renderableIndex] != 0) {
                
                // set texture
                this->m_texture = this->m_engineInstance
                    ->GetEntityManager()
                    ->GetEntity(this->m_renderableEntityList[this->m_renderableIndex])
                    ->GetComponent<Component::TextureComponent>();
                
                if (this->m_texture != NULL) {
                    sceGuTexMode(GU_PSM_8888, 0, 0, 0);
                    sceGuTexImage(
                        0, 
                        this->m_texture->GetWidth(), 
                        this->m_texture->GetHeight(), 
                        this->m_texture->GetWidth(), 
                        this->m_texture->GetRGBA()
                    );
                    sceGuTexFunc(GU_TFX_REPLACE,GU_TCC_RGBA);
                    sceGuTexEnvColor(0xffff00);
                    sceGuTexFilter(GU_LINEAR,GU_LINEAR);
                    sceGuTexScale(1.0f,1.0f);
                    sceGuTexOffset(0.0f,0.0f);
                    sceGuAmbientColor(0xffffffff);
                }

                // set model
                this->m_model = this->m_engineInstance
                    ->GetEntityManager()
                    ->GetEntity(this->m_renderableEntityList[this->m_renderableIndex])
                    ->GetComponent<Component::ModelComponent>();

                if (this->m_model != NULL) {

                    // setup matrices for cube
                    sceGumMatrixMode(GU_MODEL);
                    sceGumLoadIdentity();
                    {
                        ScePspFVector3 pos = { 0, 0, -1.5f };
                        ScePspFVector3 rot = { val * (m_renderableIndex + 1) * 0.79f * (GU_PI/180.0f), val * (m_renderableIndex + 1) * 0.98f * (GU_PI/180.0f), val * (m_renderableIndex + 1) * 1.32f * (GU_PI/180.0f) };
                        sceGumTranslate(&pos);
                        sceGumRotateXYZ(&rot);
                    }

                    this->m_engineInstance->GetRender()->DrawArray(
                        this->m_model->GetPrimitiveType(),
                        GU_TRANSFORM_3D|GU_TEXTURE_32BITF|GU_VERTEX_32BITF|GU_NORMAL_32BITF|GU_INDEX_16BIT,
                        this->m_model->GetIndicesCount(),
                        this->m_model->GetIndices(),
                        this->m_model->GetVertices()
                    );

                    val++;
                }
            }
        }

        // show FPS
        this->m_engineInstance->GetFont()->Printf(2, 14, "Framerate: %d FPS\n", (int)this->m_engineInstance->GetRender()->FPS()->GetCurrentFPS());

        // show latency
        this->m_engineInstance->GetFont()->Printf(2, 32, "Frametime: %.3f ms\n", this->m_engineInstance->GetRender()->FPS()->GetLatency());
    }

    void RenderSystem::PostUpdate()
    {
        // FPS
        this->m_engineInstance->GetRender()->FPS()->PostUpdate();

        // Swap buffers
        this->m_engineInstance->GetRender()->FrameEnd();
    }

}}} // LightSource::Game::System