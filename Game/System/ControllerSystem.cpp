#include "ControllerSystem.h"

namespace LightSource { namespace Game { namespace System {

    ControllerSystem::ControllerSystem()
    {
        sceCtrlSetSamplingCycle(0);
        sceCtrlSetSamplingMode(PSP_CTRL_MODE_ANALOG);
    }

    ControllerSystem::~ControllerSystem()
    {
    }

    void ControllerSystem::PreUpdate()
    {
        sceCtrlReadBufferPositive(&m_pad, 1);
    }

    void ControllerSystem::Update()
    {
        if (m_pad.Buttons != 0){
            if (m_pad.Buttons & PSP_CTRL_SELECT) pspDebugScreenPrintf("PSP_CTRL_SELECT\n");
            if (m_pad.Buttons & PSP_CTRL_START) pspDebugScreenPrintf("PSP_CTRL_START\n");
            if (m_pad.Buttons & PSP_CTRL_UP) pspDebugScreenPrintf("PSP_CTRL_UP\n");
            if (m_pad.Buttons & PSP_CTRL_RIGHT) pspDebugScreenPrintf("PSP_CTRL_RIGHT\n");
            if (m_pad.Buttons & PSP_CTRL_DOWN) pspDebugScreenPrintf("PSP_CTRL_DOWN\n");
            if (m_pad.Buttons & PSP_CTRL_LEFT) pspDebugScreenPrintf("PSP_CTRL_LEFT\n");
            if (m_pad.Buttons & PSP_CTRL_LTRIGGER) pspDebugScreenPrintf("PSP_CTRL_LTRIGGER\n");
            if (m_pad.Buttons & PSP_CTRL_RTRIGGER) pspDebugScreenPrintf("PSP_CTRL_RTRIGGER\n");
            if (m_pad.Buttons & PSP_CTRL_TRIANGLE) pspDebugScreenPrintf("PSP_CTRL_TRIANGLE\n");
            if (m_pad.Buttons & PSP_CTRL_CIRCLE) pspDebugScreenPrintf("PSP_CTRL_CIRCLE\n");
            if (m_pad.Buttons & PSP_CTRL_CROSS) pspDebugScreenPrintf("PSP_CTRL_CROSS\n");
            if (m_pad.Buttons & PSP_CTRL_SQUARE) pspDebugScreenPrintf("PSP_CTRL_SQUARE\n");
            if (m_pad.Buttons & PSP_CTRL_HOME) pspDebugScreenPrintf("PSP_CTRL_HOME\n");
            if (m_pad.Buttons & PSP_CTRL_HOLD) pspDebugScreenPrintf("PSP_CTRL_HOLD\n");
            if (m_pad.Buttons & PSP_CTRL_NOTE) pspDebugScreenPrintf("PSP_CTRL_NOTE\n");
            if (m_pad.Buttons & PSP_CTRL_SCREEN) pspDebugScreenPrintf("PSP_CTRL_SCREEN\n");
            if (m_pad.Buttons & PSP_CTRL_VOLUP) pspDebugScreenPrintf("PSP_CTRL_VOLUP\n");
            if (m_pad.Buttons & PSP_CTRL_VOLDOWN) pspDebugScreenPrintf("PSP_CTRL_VOLDOWN\n");
            if (m_pad.Buttons & PSP_CTRL_WLAN_UP) pspDebugScreenPrintf("PSP_CTRL_WLAN_UP\n");
            if (m_pad.Buttons & PSP_CTRL_REMOTE) pspDebugScreenPrintf("PSP_CTRL_REMOTE\n");
            if (m_pad.Buttons & PSP_CTRL_DISC) pspDebugScreenPrintf("PSP_CTRL_DISC\n");
            if (m_pad.Buttons & PSP_CTRL_MS) pspDebugScreenPrintf("PSP_CTRL_MS\n");
        }
    }

    void ControllerSystem::PostUpdate()
    {

    }

}}} // LightSource::Game::System