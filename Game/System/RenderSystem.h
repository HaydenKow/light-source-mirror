#ifndef LIGHT_SOURCE_GAME_SYSTEM_RENDERSYSTEM_H
#define LIGHT_SOURCE_GAME_SYSTEM_RENDERSYSTEM_H

#include "../../Core/LightSource.h"
#include "../../Core/_ECS_/System/SystemFamily.h"
#include "../../Core/_ECS_/System/ISystem.h"

#include "../Components/ModelComponent.h"
#include "../Components/TextureComponent.h"

namespace LightSource { namespace Game { namespace System {

    class RenderSystem : public Core::ECS::System::SystemFamily<RenderSystem>, public Core::ECS::System::ISystem
    {
    private:
        const Core::Engine*                 m_engineInstance;
        
        std::vector<EntityId>               m_renderableEntityList;
        unsigned int                        m_renderableIndex;

        Component::ModelComponent*          m_model;
        Component::TextureComponent*        m_texture;

        int val;
        
    public:
        RenderSystem(const Core::Engine* engine);
        ~RenderSystem();

        std::vector<EntityId>::const_iterator Find(const EntityId entityId);
        
        bool Exists(const EntityId entityId);
        void Add(const EntityId entityId);
        void Clear();

        virtual void PreUpdate();
        virtual void Update();
        virtual void PostUpdate();
    };

}}} // LightSource::Game::System

#endif