#ifndef LIGHT_SOURCE_GAME_GAMEBASE_H
#define LIGHT_SOURCE_GAME_GAMEBASE_H

#include "../Core/LightSource.h"

#include "GameManager.h"

namespace LightSource { namespace Game {

    class GameBase : public Core::Engine
    {
    private:
        bool m_isRunning;

        GameManager* m_gameManager;
        
    public:
        GameBase();
        ~GameBase();

        bool IsRunning() const;
    };

}} // LightSource::Game

#endif