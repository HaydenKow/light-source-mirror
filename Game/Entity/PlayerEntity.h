#ifndef LIGHT_SOURCE_GAME_ENTITY_PLAYERENTITY_H
#define LIGHT_SOURCE_GAME_ENTITY_PLAYERENTITY_H

#include "../../Core/_ECS_/Entity/IEntity.h"

#include "../Components/ModelComponent.h"
#include "../Components/TextureComponent.h"

#include "../File/OBJ/OBJLoaderText.h"
#include "../File/PNG/PNGLoader.h"

namespace LightSource { namespace Game { namespace Entity {

    class PlayerEntity : public Core::ECS::Entity::IEntity
    {
    private:
        Component::ModelComponent* m_model;
        Component::TextureComponent* m_texture;

    public:
        PlayerEntity();
        ~PlayerEntity();
    };

}}} // LightSource::Game::Entity

#endif