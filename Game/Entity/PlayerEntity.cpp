#include "PlayerEntity.h"

namespace LightSource { namespace Game { namespace Entity {

    PlayerEntity::PlayerEntity()
    {
        /*
        |-----------------------------------------------------------------------------------
        | Model
        |------------------------------------------------------------------------------
        |
        |
        |
        */

        this->m_model = NULL;
        
        File::Obj::OBJLoaderText* obj_loader = new File::Obj::OBJLoaderText();
        
        obj_loader->Load("Assets/Models/wmycr.obj");
        if (obj_loader->Read() == 0) {
            this->m_model = AddComponent<Component::ModelComponent>(obj_loader->GetIndicesCount(), obj_loader->GetVerticesCount());
            this->m_model->SetPrimitiveType(Component::ModelComponent::PrimitiveTypes::TRIANGLES);
            this->m_model->AddIndices(obj_loader->GetIndices());
            this->m_model->AddVertices(obj_loader->GetVertices());
        }
        obj_loader->Close();

        /*
        |-----------------------------------------------------------------------------------
        | Texture
        |------------------------------------------------------------------------------
        |
        |
        |
        */

        this->m_texture = NULL;
        
        File::Png::PNGLoader* png_loader = new File::Png::PNGLoader();
        
        if (png_loader->Load("Assets/Textures/WMYCR.png")) {
            this->m_texture = AddComponent<Component::TextureComponent>(png_loader->GetWidth(), png_loader->GetHeight());
            this->m_texture->SetRGBA((TextureRGBA*)png_loader->GetRGBA());
        }
    }

    PlayerEntity::~PlayerEntity()
    {
    }

}}} // LightSource::Game::Entity