#include "TextureComponent.h"

namespace LightSource { namespace Game { namespace Component {

    TextureComponent::TextureComponent(int width, int height)
    {
        this->m_width = width;
        this->m_height = height;
        this->m_texture = (TextureRGBA*)calloc(width * height * 4, sizeof(TextureRGBA));
    }

    TextureComponent::~TextureComponent()
    {
    }

    void TextureComponent::SetRGBA(TextureRGBA* texture)
    {
        for (int i = (this->m_width * this->m_height * 4) - 1; i >= 0; i--) {
            this->m_texture[i].r = texture[i].r;
            this->m_texture[i].g = texture[i].g;
            this->m_texture[i].b = texture[i].b;
            this->m_texture[i].a = texture[i].a;
        }
    }

    int TextureComponent::GetWidth() const
    {
        return this->m_width;
    }

    int TextureComponent::GetHeight() const
    {
        return this->m_height;
    }

    TextureRGBA* TextureComponent::GetRGBA() const
    {
        return this->m_texture;
    }

}}} // LightSource::Game::Component