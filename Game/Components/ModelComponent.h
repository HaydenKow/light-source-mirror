#ifndef LIGTH_SOURCE_GAME_COMPONENT_MODELCOMPONENT_H
#define LIGTH_SOURCE_GAME_COMPONENT_MODELCOMPONENT_H

#include <pspgu.h>

#include "../Core/_ECS_/Components/ComponentFamily.h"
#include "../Core/_ECS_/Components/IComponent.h"

namespace LightSource { namespace Game { namespace Component {

    class ModelComponent : public Core::ECS::Components::ComponentFamily<ModelComponent>, public Core::ECS::Components::IComponent
    {
    private:
        unsigned int            m_verticesCount;
        unsigned int            m_indicesCount;

        int                     m_primitiveType;
        unsigned short*         m_indices __attribute__((aligned(16)));
        Vector32BITF*           m_vertices __attribute__((aligned(16)));

    public:
        ModelComponent(unsigned int vertexCount);
        ModelComponent(unsigned int indexCount, unsigned int vertexCount);
        ~ModelComponent();

        void SetPrimitiveType(const int prim);
        
        void AddIndices(const unsigned short* indices);
        void AddVertices(const Vector32BITF* vertices);

        int GetPrimitiveType() const;
        void* GetIndices() const;
        void* GetVertices() const;

        unsigned int GetIndicesCount() const;
        unsigned int GetVerticesCount() const;

        struct PrimitiveTypes
        {
            enum
            {
                POINTS = GU_POINTS,
                LINES = GU_LINES,
                LINE_STRIP = GU_LINE_STRIP,
                TRIANGLES = GU_TRIANGLES,
                TRIANGLE_STRIP = GU_TRIANGLE_STRIP,
                TRIANGLE_FAN = GU_TRIANGLE_FAN,
                SPRITES = GU_SPRITES
            };
        };
    };

}}} // LightSource::Game::Component

#endif