#ifndef LIGTH_SOURCE_GAME_COMPONENT_TEXTURECOMPONENT_H
#define LIGTH_SOURCE_GAME_COMPONENT_TEXTURECOMPONENT_H

#include "../Core/_ECS_/Components/ComponentFamily.h"
#include "../Core/_ECS_/Components/IComponent.h"

namespace LightSource { namespace Game { namespace Component {

    class TextureComponent : public Core::ECS::Components::ComponentFamily<TextureComponent>, public Core::ECS::Components::IComponent
    {
    private:
        int                 m_width;
        int                 m_height;
        TextureRGBA*        m_texture;

    public:
        TextureComponent(int width, int height);
        ~TextureComponent();

        void SetRGBA(TextureRGBA* texture);

        int GetWidth() const;
        int GetHeight() const;
        TextureRGBA* GetRGBA() const;
    };

}}} // LightSource::Game::Component

#endif