#include "ModelComponent.h"

namespace LightSource { namespace Game { namespace Component {

    ModelComponent::ModelComponent(unsigned int vertexCount)
    {
        this->m_indicesCount = 0;
        this->m_verticesCount = vertexCount;
    }

    ModelComponent::ModelComponent(unsigned int indexCount, unsigned int vertexCount)
    {
        this->m_indicesCount = indexCount;
        this->m_verticesCount = vertexCount;

        this->m_indices = (unsigned short*)calloc(indexCount, sizeof(short));
        this->m_vertices = (Vector32BITF*)calloc(vertexCount, sizeof(Vector32BITF));
    }

    ModelComponent::~ModelComponent()
    {
    }

    void ModelComponent::SetPrimitiveType(const int prim)
    {
        this->m_primitiveType = prim;
    }

    void ModelComponent::AddIndices(const unsigned short* indices)
    {
        for (int i = this->m_indicesCount - 1; i >= 0; i--) {
            this->m_indices[i] = indices[i];
        }
    }

    void ModelComponent::AddVertices(const Vector32BITF* vertices)
    {
        for (int i = this->m_verticesCount - 1; i >= 0; i--) {
            ((Vector32BITF*)((int)this->m_vertices + sizeof(Vector32BITF) * i))->u = ((Vector32BITF*)((int)vertices + sizeof(Vector32BITF) * i))->u;
            ((Vector32BITF*)((int)this->m_vertices + sizeof(Vector32BITF) * i))->v = ((Vector32BITF*)((int)vertices + sizeof(Vector32BITF) * i))->v;

            ((Vector32BITF*)((int)this->m_vertices + sizeof(Vector32BITF) * i))->nx = ((Vector32BITF*)((int)vertices + sizeof(Vector32BITF) * i))->nx;
            ((Vector32BITF*)((int)this->m_vertices + sizeof(Vector32BITF) * i))->ny = ((Vector32BITF*)((int)vertices + sizeof(Vector32BITF) * i))->ny;
            ((Vector32BITF*)((int)this->m_vertices + sizeof(Vector32BITF) * i))->nz = ((Vector32BITF*)((int)vertices + sizeof(Vector32BITF) * i))->nz;

            ((Vector32BITF*)((int)this->m_vertices + sizeof(Vector32BITF) * i))->x = ((Vector32BITF*)((int)vertices + sizeof(Vector32BITF) * i))->x;
            ((Vector32BITF*)((int)this->m_vertices + sizeof(Vector32BITF) * i))->y = ((Vector32BITF*)((int)vertices + sizeof(Vector32BITF) * i))->y;
            ((Vector32BITF*)((int)this->m_vertices + sizeof(Vector32BITF) * i))->z = ((Vector32BITF*)((int)vertices + sizeof(Vector32BITF) * i))->z;
        }
    }

    int ModelComponent::GetPrimitiveType() const
    {
        return this->m_primitiveType;
    }

    void* ModelComponent::GetIndices() const
    {
        return (void*)this->m_indices;
    }

    void* ModelComponent::GetVertices() const
    {
        return (void*)this->m_vertices;
    }

    unsigned int ModelComponent::GetIndicesCount() const
    {
        return this->m_indicesCount;
    }

    unsigned int ModelComponent::GetVerticesCount() const
    {
        return this->m_verticesCount;
    }

}}} // LightSource::Game::Component