#include "GameBase.h"

PSP_MODULE_INFO("[PSP] Half-Life", 0, 1, 1);
PSP_MAIN_THREAD_ATTR(THREAD_ATTR_USER | THREAD_ATTR_VFPU);

using namespace LightSource::Game;

int main(void)
{
	pspDebugScreenInit();

	// game
	GameBase* game = new GameBase();

	while(game->IsRunning()) {
		game->GetSystemManager()->Update();
	}

	return 0;
}
