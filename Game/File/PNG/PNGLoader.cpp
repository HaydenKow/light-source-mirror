#include "PNGLoader.h"

namespace LightSource { namespace Game { namespace File { namespace Png {

    PNGLoader::PNGLoader()
    {
        m_good = false;
        m_width = 0;
        m_height = 0;
        m_rgba = 0;
    }

    PNGLoader::~PNGLoader()
    {
        if (m_rgba)
        {
            delete  m_rgba;
        }
    }

    bool PNGLoader::Load(const char* path)
    {
        // reset health flag
        m_good = false;

        // check filetype
        if (!DoCheckFileHeader(path)) return false;

        // try to open file
        FILE* file = fopen(path, "rb");

        // unable to open
        if (file == 0) return false;

        // create read struct
        png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);

        // check pointer
        if (png_ptr == 0)
        {
            fclose(file);
            return false;
        }

        // create info struct
        png_infop info_ptr = png_create_info_struct(png_ptr);

        // check pointer
        if (info_ptr == 0)
        {
            png_destroy_read_struct(&png_ptr, 0, 0);
            fclose(file);
            return false;
        }

        // set error handling
        if (setjmp(png_jmpbuf(png_ptr)))
        {
            png_destroy_read_struct(&png_ptr, &info_ptr, 0);
            fclose(file);
            return false;
        }

        // I/O initialization using standard C streams
        png_init_io(png_ptr, file);

        // read entire image (high level)
        png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_EXPAND, 0);

        // convert the png bytes to rgba
        if (!DoExtractCanonicData(png_ptr, info_ptr))
        {
            png_destroy_read_struct(&png_ptr, &info_ptr, 0);

            fclose(file);
            return false;
        }

        // free memory
        png_destroy_read_struct(&png_ptr, &info_ptr, 0);

        // close file
        fclose(file);

        // reset health flag
        return (m_good = true);
    }

    bool PNGLoader::Good() const
    {
        return m_good;
    }

    int PNGLoader::GetWidth() const
    {
        return m_width;
    }

    int PNGLoader::GetHeight() const
    {
        return m_height;
    }

    unsigned char* PNGLoader::GetRGBA() const
    {
        return m_rgba;
    }

    bool PNGLoader::DoCheckFileHeader(const char* path) const
    {
        // try to open file
        FILE* file = fopen(path, "rb");

        // unable to open
        if (file == 0) return false;

        // the number of bytes to read
        const unsigned int nread = 8;

        unsigned char buffer[nread];

        // try to read header
        if (fread(buffer, 1, nread, file) != nread)
        {
            fclose(file);
            return 0;
        }

        // close file
        fclose(file);

        // compare headers
        int result = png_sig_cmp(buffer, 0, nread);

        return(result == 0);
    }

    bool PNGLoader::DoExtractCanonicData(png_structp& pngPtr, png_infop& infoPtr)
    {
        // check pointer
        if (m_rgba)
        {
            // free memory
            delete m_rgba;

            // reset pointer
            m_rgba = 0;
        }

        // get dimensions
        m_width = png_get_image_width(pngPtr, infoPtr);
        m_height = png_get_image_height(pngPtr, infoPtr);

        // get color information
        int color_type = png_get_color_type(pngPtr, infoPtr);

        // rgb
        if (color_type == PNG_COLOR_TYPE_RGB)
        {
            m_rgba = DoConvertRGB8(pngPtr, infoPtr);
        }

        // rgb with opacity
        else if (color_type == PNG_COLOR_TYPE_RGB_ALPHA)
        {
            m_rgba = DoConvertRGBA8(pngPtr, infoPtr);
        }

        // 256 grey values
        else if (color_type == PNG_COLOR_TYPE_GRAY)
        {
            m_rgba = DoConvertGrey8(pngPtr, infoPtr);
        }

        // 256 grey values with opacity
        else if (color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
        {
            m_rgba = DoConvertGreyA8(pngPtr, infoPtr);
        }

        // check pointer
        return (m_rgba != 0);
    }

    unsigned char* PNGLoader::DoConvertRGB8(png_structp& pngPtr, png_infop& infoPtr) const
    {
        // calculate needed memory
        int size = m_height * m_width * 4;

        // allocate memory
        unsigned char* rgba = new unsigned char[size];

        // get image rows
        png_bytep* row_pointers = png_get_rows(pngPtr, infoPtr);

        int pos = 0;

        // get color values
        for(int i = 0; i < m_height; i++)
        {
            for(int j = 0; j < (3 * m_width); j += 3)
            {
                rgba[pos++] = row_pointers[i][j];	    // red
                rgba[pos++] = row_pointers[i][j + 1];	// green
                rgba[pos++] = row_pointers[i][j + 2];	// blue
                rgba[pos++] = 0;						// alpha
            }
        }

        return rgba;
    }

    unsigned char* PNGLoader::DoConvertRGBA8(png_structp& pngPtr, png_infop& infoPtr) const
    {
        // calculate needed memory
        int size = m_height * m_width * 4;

        // allocate memory
        unsigned char* rgba = new unsigned char[size];

        // get image rows
        png_bytep* row_pointers = png_get_rows(pngPtr, infoPtr);

        int pos = 0;

        // get color values
        for(int i = 0; i < m_height; i++)
        {
            for(int j = 0; j < (4 * m_width); j += 4)
            {
                rgba[pos++] = row_pointers[i][j];	    // red
                rgba[pos++] = row_pointers[i][j + 1];	// green
                rgba[pos++] = row_pointers[i][j + 2];	// blue
                rgba[pos++] = row_pointers[i][j + 3];	// alpha
            }
        }

        return rgba;
    }

    unsigned char* PNGLoader::DoConvertGrey8(png_structp& pngPtr, png_infop& infoPtr) const
    {
        // calculate needed memory
        int size = m_height * m_width * 4;

        // allocate memory
        unsigned char* rgba = new unsigned char[size];

        // get image rows
        png_bytep* row_pointers = png_get_rows(pngPtr, infoPtr);

        int pos = 0;

        // get color values
        for(int i = 0; i < m_height; i++)
        {
            for(int j = 0; j < m_width; j++)
            {
                rgba[pos++] = row_pointers[i][j];	// blue
                rgba[pos++] = row_pointers[i][j];	// green
                rgba[pos++] = row_pointers[i][j];	// red
                rgba[pos++] = 0;					// alpha
            }
        }

        return rgba;
    }

    unsigned char* PNGLoader::DoConvertGreyA8(png_structp& pngPtr, png_infop& infoPtr) const
    {
        // calculate needed memory
        int size = m_height * m_width * 4;

        // allocate memory
        unsigned char* rgba = new unsigned char[size];

        // get image rows
        png_bytep* row_pointers = png_get_rows(pngPtr, infoPtr);

        int pos = 0;

        // get color values
        for(int i = 0; i < m_height; i++)
        {
            for(int j = 0; j < (2 * m_width); j += 2)
            {
                rgba[pos++] = row_pointers[i][j];		// blue
                rgba[pos++] = row_pointers[i][j];		// green
                rgba[pos++] = row_pointers[i][j];		// red
                rgba[pos++] = row_pointers[i][j + 1];	// alpha
            }
        }

        return rgba;
    }

}}}} // LightSource::Game::File::Png