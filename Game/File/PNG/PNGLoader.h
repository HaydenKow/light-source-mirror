/*
 * Based on: Texture version 0.2 by Achim Klein
 */

#ifndef LIGHT_SOURCE_GAME_FILE_PNG_PNGLOADER_H
#define LIGHT_SOURCE_GAME_FILE_PNG_PNGLOADER_H

#include <png.h>

#include <stdio.h>
#include <stdlib.h>

#include "../../../Core/_ECS_/API.h"

namespace LightSource { namespace Game { namespace File { namespace Png {

class PNGLoader
{
    private:
        bool                m_good;
        int                 m_width;
        int                 m_height;
        unsigned char*      m_rgba;

    protected:
        /**
         * Returns true if the specified file is a png file.
         */
        bool DoCheckFileHeader(const char* path) const;
        /**
         * Converts the png bytes to RGBA.
         *
         * @par Assumptions
         * - png_read_png() has been called with the PNG_TRANSFORM_EXPAND flag
         */
        bool DoExtractCanonicData(png_structp& pngPtr, png_infop& infoPtr);
        /**
         * Gets the data from an 8-bit rgb image.
         *
         * @par Memory
         * - The returned pointer is created by using the new] operator.
         * - You have to free the allocated memory yourself.
         *
         * @par Structure
         * - The color-sequence is Blue-Green-Red-Alpha (8 bit each).
         * - The first 4 values (RGBA) are located in the top-left corner of the image.
         * - The last 4 values (RGBA) are located in the bottom-right corner of the image.
         */
        unsigned char* DoConvertRGB8(png_structp& pngPtr, png_infop& infoPtr) const;
        /**
         * Gets the data from an 8-bit rgb image with alpha values.
         *
         * @par Memory
         * - The returned pointer is created by using the new] operator.
         * - You have to free the allocated memory yourself.
         *
         * @par Structure
         * - The color-sequence is Blue-Green-Red-Alpha (8 bit each).
         * - The first 4 values (RGBA) are located in the top-left corner of the image.
         * - The last 4 values (RGBA) are located in the bottom-right corner of the image.
         */
        unsigned char* DoConvertRGBA8(png_structp& pngPtr, png_infop& infoPtr) const;
        /**
         * Gets the data from an 8-bit monochrome image.
         *
         * @par Memory
         * - The returned pointer is created by using the new] operator.
         * - You have to free the allocated memory yourself.
         *
         * @par Structure
         * - The color-sequence is Blue-Green-Red-Alpha (8 bit each).
         * - The first 4 values (RGBA) are located in the top-left corner of the image.
         * - The last 4 values (RGBA) are located in the bottom-right corner of the image.
         */
        unsigned char* DoConvertGrey8(png_structp& pngPtr, png_infop& infoPtr) const;
        /**
         * Gets the data from an 8-bit monochrome image with alpha values.
         */
        unsigned char* DoConvertGreyA8(png_structp& pngPtr, png_infop& infoPtr) const;

    public:
        PNGLoader();
        virtual ~PNGLoader();

        /**
         * Reads a png file.
         *
         * @return true if the file operation succeeded
         * @return false if the file operation failed
         */
        bool Load(const char* path);
        /**
         * Returns true if the object contains valid data.
         */
        bool Good() const;

        /**
         * Returns the image's width.
         */
        int GetWidth() const;
        /**
         * Returns the image's height.
         */
        int GetHeight() const;
        /**
         * Returns the image's RGB+Alpha values (8 bit each).
         *
         * @par Memory
         * - Do not delete the returned pointer yourself. It will be deleted internally.
         *
         * @par Structure
         * - The color sequence is Blue-Green-Red-Alpha (8 bit each).
         * - The first 4 values (rgba) are located in the top-left corner of the image.
         * - The last 4 values (rgba) are located in the bottom-right corner of the image.
         * - The number of elements is always: getWidth() * getHeight() * 4
         *
         * @par Customizing the structure
         * If you constantly need another color sequence than rgba (for example RGB),
         * it should be better (faster) to change the internal conversion routines
         * than to change the byte order in the returned array.
         *
         * @see
         * doConvertRGB8()
         * doConvertRGBA8()
         * doConvertGrey8()
         * doConvertGreyA8()
         */
        unsigned char* GetRGBA() const;
};

}}}} // LightSource::Game::File::Png

#endif