#ifndef LIGHT_SOURCE_GAME_FILE_OBJ_OBJLOADERTEXT_H
#define LIGHT_SOURCE_GAME_FILE_OBJ_OBJLOADERTEXT_H

#include "../../../Core/_ECS_/API.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>

namespace LightSource { namespace Game { namespace File { namespace Obj {

    class OBJLoaderText
    {
    private:
        FILE*                   m_file;

        int                     m_primitiveType;

        SceUInt32               m_indicesCount;
        SceUInt32               m_verticesCount;

        SceUShort16*            m_indices __attribute__((aligned(16)));
        Vector32BITF*           m_vertices __attribute__((aligned(16)));

    public:
        OBJLoaderText();
        ~OBJLoaderText();

        int GetPrimitiveType() const;
        SceUInt32 GetIndicesCount() const;
        SceUInt32 GetVerticesCount() const;
        SceUShort16* GetIndices() const;
        Vector32BITF* GetVertices() const;

        void Load(const char* file);
        int Read();
        void Close();
    };

}}}} // LightSource::Game::File::Obj

#endif