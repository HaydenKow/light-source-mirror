#include "OBJLoaderText.h"

namespace LightSource { namespace Game { namespace File { namespace Obj {

    OBJLoaderText::OBJLoaderText()
    {
    }

    OBJLoaderText::~OBJLoaderText()
    {
    }

    int OBJLoaderText::GetPrimitiveType() const
    {
        return this->m_primitiveType;
    }

    SceUInt32 OBJLoaderText::GetIndicesCount() const
    {
        return this->m_indicesCount;
    }

    SceUInt32 OBJLoaderText::GetVerticesCount() const
    {
        return this->m_verticesCount;
    }

    SceUShort16* OBJLoaderText::GetIndices() const
    {
        return this->m_indices;
    }

    Vector32BITF* OBJLoaderText::GetVertices() const
    {
        return this->m_vertices;
    }
    
    void OBJLoaderText::Load(const char* file)
    {
        this->m_file = fopen(file, "r");
    }

    int OBJLoaderText::Read()
    {
        if (this->m_file != NULL) {
            pspDebugScreenPrintf("Loading: FILE* 0x%08x\n", (int)this->m_file);

            // Step 1: read count vertices, vtable, vn and faces

            fseek(this->m_file, 0, SEEK_SET);

            char lineHeader[1024];
            this->m_indicesCount = 0;
            this->m_verticesCount = 0;
            int vtableCount = 0, normalCount = 0;

            while (fgets(lineHeader, 1024, this->m_file)) {
                if (strncmp("vt", lineHeader, 2) == 0) {
                    vtableCount++;
                } else if (strncmp("vn", lineHeader, 2) == 0) {
                    normalCount++;
                } else if (strncmp("v", lineHeader, 1) == 0) {
                    this->m_verticesCount++;
                } else if (strncmp("f", lineHeader, 1) == 0) {
                    this->m_indicesCount += 3;
                }
            }
            pspDebugScreenPrintf(
                "Counts: \nv: %d\nvt: %d\nvn: %d\nf: %d\n", 
                this->m_verticesCount,
                vtableCount,
                normalCount,
                this->m_indicesCount
            );

            // Step 2: init array and read again

            fseek(this->m_file, 0, SEEK_SET);
            
            this->m_indices = (SceUShort16*)calloc(this->m_indicesCount, sizeof(SceUShort16));
            this->m_vertices = (Vector32BITF*)calloc(this->m_verticesCount, sizeof(Vector32BITF));
            ScePspFVector2* vtable = (ScePspFVector2*)calloc(vtableCount, sizeof(ScePspFVector2));
            ScePspFVector3* normal = (ScePspFVector3*)calloc(normalCount, sizeof(ScePspFVector3));
            int v_index = 0, vt_index = 0, vn_index = 0, f_index = 0;
            unsigned int vertex_i[3], vtable_i[3], vnormal_i[3];

            while (fgets(lineHeader, 1024, this->m_file)) 
            {
                if (strncmp("vt", lineHeader, 2) == 0) 
                {
                    if (sscanf(lineHeader, "vt %f %f\n", 
                        &vtable[vt_index].x, 
                        &vtable[vt_index].y
                    ) != 2) return -2;
                    
                    vt_index++;
                }
                else if (strncmp("vn", lineHeader, 2) == 0) 
                {
                    if (sscanf(lineHeader, "vn %f %f %f\n", 
                        &normal[vn_index].x, 
                        &normal[vn_index].y, 
                        &normal[vn_index].z
                    ) != 3) return -3;
                    
                    vn_index++;
                }
                else if (strncmp("v", lineHeader, 1) == 0) 
                {
                    if (sscanf(lineHeader, "v %f %f %f\n", 
                        &m_vertices[v_index].x,
                        &m_vertices[v_index].y, 
                        &m_vertices[v_index].z
                    ) != 3) return -1;
                    
                    v_index++;
                } 
                else if (strncmp("f", lineHeader, 1) == 0) 
                {
                    if (sscanf(lineHeader, "f %d/%d/%d %d/%d/%d %d/%d/%d\n",
                        &vertex_i[0], &vtable_i[0], &vnormal_i[0],
                        &vertex_i[1], &vtable_i[1], &vnormal_i[1],
                        &vertex_i[2], &vtable_i[2], &vnormal_i[2]
                    ) != 9) return -4;

                    for(unsigned int i = 0; i < 3; i++) {
                        // indices
                        this->m_indices[f_index * 3 + i] = vertex_i[i] - 1;
                        
                        // vertices
                        this->m_vertices[vertex_i[i] - 1].u = vtable[vtable_i[i] - 1].x;
                        this->m_vertices[vertex_i[i] - 1].v = vtable[vtable_i[i] - 1].y;

                        // normals
                        this->m_vertices[vertex_i[i] - 1].nx = normal[vnormal_i[i] - 1].x;
                        this->m_vertices[vertex_i[i] - 1].ny = normal[vnormal_i[i] - 1].y;
                        this->m_vertices[vertex_i[i] - 1].nz = normal[vnormal_i[i] - 1].z;
                    }

                    f_index++;
                }
            }
        }

        return 0;
    }

    void OBJLoaderText::Close()
    {
        fclose(this->m_file);
    }

}}}} // LightSource::Game::File::Obj