#ifndef LIGHT_SOURCE_GAME_FILE_INTERFACELOADER_H
#define LIGHT_SOURCE_GAME_FILE_INTERFACELOADER_H

#include <stdio.h>

namespace LightSource { namespace Game { namespace File {

    class ILoader
    {
    private:
        FILE* m_file;

    public:
        ILoader();
        virtual ~ILoader();

        virtual void Load(const char* file) = 0;
        virtual void Read() = 0;
        virtual void Close() = 0;
    };

}}} // LightSource::Game::File

#endif