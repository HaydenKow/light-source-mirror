#ifndef LIGHT_SOURCE_GAME_GAMEMANAGER_H
#define LIGHT_SOURCE_GAME_GAMEMANAGER_H

#include "../Core/LightSource.h"

#include "System/ControllerSystem.h"
#include "System/RenderSystem.h"

#include "Entity/PlayerEntity.h"

namespace LightSource { namespace Game {

    class GameManager
    {
    private:
        const Core::Engine* m_engineInstance;

        System::RenderSystem* m_renderSystem;

    public:
        GameManager(const Core::Engine* engine);
        ~GameManager();
    };

}} // LightSource::Game

#endif