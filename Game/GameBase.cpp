#include "GameBase.h"

namespace LightSource { namespace Game {

    GameBase::GameBase()
    {
        this->m_gameManager = new GameManager(this);

        this->m_isRunning = true;
    }

    GameBase::~GameBase()
    {
        this->m_isRunning = false;
    }

    bool GameBase::IsRunning() const
    {
        return this->m_isRunning;
    }

}} // LightSource::Game