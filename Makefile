# The name of application
TARGET := [PSP]Half-Life

# Main build folder
LS_BUILD_DIR := _build

# Folders with .c and .cpp files
LS_FOLDERS := Core \
Core/_ECS_ \
Core/_ECS_/Components \
Core/_ECS_/Entity \
Core/_ECS_/Events \
Core/_ECS_/Memory \
Core/_ECS_/System \
Core/_ECS_/Utils \
Core/Graphics \
Core/Physics \
Core/Scripting \
Core/Sounds \
Core/System \
Game \
Game/Components \
Game/Entity \
Game/File \
Game/File/OBJ \
Game/File/PNG \
Game/System

#------------------------------------------------
# Pre-build config
#--------------------------------------------

SOURCES_PATTERN := *.cpp
SOURCES := $(wildcard $(addsuffix /$(SOURCES_PATTERN),$(LS_FOLDERS)))

OBJS := $(subst .cpp,.o,$(SOURCES))

#------------------------------------------------
# Compiler confing
#--------------------------------------------

INCDIR = $(LS_FOLDERS)
CFLAGS = -O2 -G0 -Wall -fno-strict-aliasing
CXXFLAGS = $(CFLAGS) -std=c++0x -fno-exceptions -fno-rtti
ASFLAGS = $(CFLAGS)

ifeq ($(PSP_FAT), 1)
CXXFLAGS += -D PSP_FAT
endif

BUILD_PRX=1
PSP_FW_VERSION=371
PSP_LARGE_MEMORY=1

LIBDIR = Core/_Libs_
LDFLAGS =
LIBS = -lstdc++ -lintrafont -lpspgum -lpspgu -lpsprtc -lpspfpu -lpng -ljpeg -lm -lz

BDIR := $(LS_BUILD_DIR)
EXTRA_TARGETS = $(BDIR)/EBOOT.PBP

PSPSDK=$(shell psp-config --pspsdk-path)

INCDIR += $(PSPSDK)/psp/include
LIBDIR += $(PSPSDK)/psp/lib

include psp-builder.mak